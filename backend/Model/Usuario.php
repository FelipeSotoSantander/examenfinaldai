<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author fsoto
 */
class Usuario {

    //put your code here

    private $id_usuario;
    private $id_perfil;
    private $nombre_usuario;
    private $login_usuario;
    private $clave_usuario;
    private $estado_usuario;
    private $rut;

    function __construct() {
        
    }

    function getId_usuario() {
        return $this->id_usuario;
    }

    function getId_perfil() {
        return $this->id_perfil;
    }

    function getNombre_usuario() {
        return $this->nombre_usuario;
    }

    function getLogin_usuario() {
        return $this->login_usuario;
    }

    function getClave_usuario() {
        return $this->clave_usuario;
    }

    function getEstado_usuario() {
        return $this->estado_usuario;
    }

    function getRut() {
        return $this->rut;
    }

    function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
    }

    function setNombre_usuario($nombre_usuario) {
        $this->nombre_usuario = $nombre_usuario;
    }

    function setLogin_usuario($login_usuario) {
        $this->login_usuario = $login_usuario;
    }

    function setClave_usuario($clave_usuario) {
        $this->clave_usuario = $clave_usuario;
    }

    function setEstado_usuario($estado_usuario) {
        $this->estado_usuario = $estado_usuario;
    }

    function setRut($rut) {
        $this->rut = $rut;
    }

    function ClaseEnArray() {
        return array(
        'id_usuario' => $this->getId_usuario(),
        'id_perfil' => $this->getId_perfil(),
        'nombre_usuario' => $this->getNombre_usuario(),
        'login_usuario' => $this->getLogin_usuario(),
        'clave_usuario' => $this->getClave_usuario(),
        'estado_usuario' => $this->getEstado_usuario(),
        'rut' => $this->getRut()
        );
    }

}
