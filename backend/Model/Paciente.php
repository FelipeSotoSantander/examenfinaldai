<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Paciente
 *
 * @author fsoto
 */
class Paciente {

    //put your code here

    private $rut;
    private $nombre_completo;
    private $fecha_de_nacimiento;
    private $sexo;
    private $direccion;
    private $telefono;

    function __construct() {
        
    }

    function getRut() {
        return $this->rut;
    }

    function getNombre_completo() {
        return $this->nombre_completo;
    }

    function getFecha_de_nacimiento() {
        return $this->fecha_de_nacimiento;
    }

    function getSexo() {
        return $this->sexo;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function setRut($rut) {
        $this->rut = $rut;
    }

    function setNombre_completo($nombre_completo) {
        $this->nombre_completo = $nombre_completo;
    }

    function setFecha_de_nacimiento($fecha_de_nacimiento) {
        $this->fecha_de_nacimiento = $fecha_de_nacimiento;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function ClaseEnArray() {
        return array(
            'rut' => $this->getRut(),
            'nombre_completo' => $this->getNombre_completo(),
            'fecha_de_nacimiento' => $this->getFecha_de_nacimiento(),
            'sexo' => $this->getSexo(),
            'direccion' => $this->getDireccion(),
            'telefono' => $this->getTelefono()
        );
    }

}
