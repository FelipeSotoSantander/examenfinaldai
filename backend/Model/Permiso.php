<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Permiso
 *
 * @author fsoto
 */
class Permiso {

    //put your code here

    private $id_permiso;
    private $id_perfil;
    private $id_privilegio;
    private $privilegio;

    function __construct() {
        
    }

    function getId_permiso() {
        return $this->id_permiso;
    }

    function getId_perfil() {
        return $this->id_perfil;
    }

    function getId_privilegio() {
        return $this->id_privilegio;
    }

    function getPrivilegio() {
        return $this->privilegio;
    }

    function setId_permiso($id_permiso) {
        $this->id_permiso = $id_permiso;
    }

    function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
    }

    function setId_privilegio($id_privilegio) {
        $this->id_privilegio = $id_privilegio;
    }

    function setPrivilegio($privilegio) {
        $this->privilegio = $privilegio;
    }

    function ClaseEnArray() {
        return array(
            'id_permiso' => $this->getId_permiso(),
            'id_perfil' => $this->getId_perfil(),
            'id_privilegio' => $this->getId_privilegio(),
            'privilegio' => $this->getPrivilegio()
        );
    }

}
