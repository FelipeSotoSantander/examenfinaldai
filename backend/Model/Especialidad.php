<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Especialidad
 *
 * @author fsoto
 */
class Especialidad {

    //put your code here

    private $especialidad;
    private $nombre_especialidad;

    function __construct() {
        
    }

    function getEspecialidad() {
        return $this->especialidad;
    }

    function getNombre_especialidad() {
        return $this->nombre_especialidad;
    }

    function setEspecialidad($especialidad) {
        $this->especialidad = $especialidad;
    }

    function setNombre_especialidad($nombre_especialidad) {
        $this->nombre_especialidad = $nombre_especialidad;
    }

    function ClaseEnArray() {
        return array(
        'especialidad' => $this->getEspecialidad(),
        'nombre_especialidad' => $this->getNombre_especialidad()
        );
    }

}
