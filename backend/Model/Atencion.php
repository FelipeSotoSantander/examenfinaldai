<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Atencion
 *
 * @author fsoto
 */
class Atencion {

    //put your code here

    private $id_atencion;
    private $fecha;
    private $paciente;
    private $medico;
    private $estado;

    function __construct() {
        
    }

    function getId_atencion() {
        return $this->id_atencion;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getPaciente() {
        return $this->paciente;
    }

    function getMedico() {
        return $this->medico;
    }

    function getEstado() {
        return $this->estado;
    }

    function setId_atencion($id_atencion) {
        $this->id_atencion = $id_atencion;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    function setPaciente($paciente) {
        $this->paciente = $paciente;
    }

    function setMedico($medico) {
        $this->medico = $medico;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function ClaseEnArray() {
        return array(
            'id_atencion' => $this->getId_atencion(),
            'fecha' => $this->getFecha(),
            'paciente' => $this->getPaciente(),
            'medico' => $this->getMedico(),
            'estado' => $this->getEstado()
        );
    }

}
