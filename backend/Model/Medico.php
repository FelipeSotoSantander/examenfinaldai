<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Medico
 *
 * @author fsoto
 */
class Medico {

    //put your code here


    private $rut_medico;
    private $nombre_medico;
    private $fecha_de_contratacion;
    private $especialidad;
    private $valor_consulta;

    function __construct() {
        
    }

    function getRut_medico() {
        return $this->rut_medico;
    }

    function getNombre_medico() {
        return $this->nombre_medico;
    }

    function getFecha_de_contratacion() {
        return $this->fecha_de_contratacion;
    }

    function getEspecialidad() {
        return $this->especialidad;
    }

    function getValor_consulta() {
        return $this->valor_consulta;
    }

    function setRut_medico($rut_medico) {
        $this->rut_medico = $rut_medico;
    }

    function setNombre_medico($nombre_medico) {
        $this->nombre_medico = $nombre_medico;
    }

    function setFecha_de_contratacion($fecha_de_contratacion) {
        $this->fecha_de_contratacion = $fecha_de_contratacion;
    }

    function setEspecialidad($especialidad) {
        $this->especialidad = $especialidad;
    }

    function setValor_consulta($valor_consulta) {
        $this->valor_consulta = $valor_consulta;
    }

    function ClaseEnArray() {
        return array(
            'rut_medico' => $this->getRut_medico(),
            'nombre_medico' => $this->getNombre_medico(),
            'fecha_de_contratacion' => $this->getFecha_de_contratacion(),
            'especialidad' => $this->getEspecialidad(),
            'valor_consulta' => $this->getValor_consulta()
        );
    }

}
