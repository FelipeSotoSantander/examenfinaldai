<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Perfil
 *
 * @author fsoto
 */
class Perfil {

    //put your code here

    private $id_perfil;
    private $nombre_perfil;

    function __construct() {
        
    }

    function getId_perfil() {
        return $this->id_perfil;
    }

    function getNombre_perfil() {
        return $this->nombre_perfil;
    }

    function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
    }

    function setNombre_perfil($nombre_perfil) {
        $this->nombre_perfil = $nombre_perfil;
    }

    function ClaseEnArray() {
        return array(
            'id_perfil' => $this->getId_perfil(),
            'nombre_perfil' => $this->getNombre_perfil()            
        );
    }

}
