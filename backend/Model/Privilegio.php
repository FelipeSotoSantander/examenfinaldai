<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Privilegio
 *
 * @author fsoto
 */
class Privilegio {

    //put your code here
    private $id_privilegio;
    private $nombre_privilegio;
    private $url_privilegio;

    function __construct() {
        
    }

    function getId_privilegio() {
        return $this->id_privilegio;
    }

    function getNombre_privilegio() {
        return $this->nombre_privilegio;
    }

    function getUrl_privilegio() {
        return $this->url_privilegio;
    }

    function setId_privilegio($id_privilegio) {
        $this->id_privilegio = $id_privilegio;
    }

    function setNombre_privilegio($nombre_privilegio) {
        $this->nombre_privilegio = $nombre_privilegio;
    }

    function setUrl_privilegio($url_privilegio) {
        $this->url_privilegio = $url_privilegio;
    }

    function ClaseEnArray() {
        return array(
            'id_privilegio' => $this->getId_privilegio(),
            'nombre_privilegio' => $this->getNombre_privilegio(),
            'url_privilegio' => $this->getUrl_privilegio()
        );
    }

}
