<?php

header('Content-type: text/html; charset=utf-8');

session_start();

include_once "/../Controller/UsuarioController.php";
include_once "/../Controller/PacienteController.php";
include_once "/../Controller/MedicoController.php";
include_once "/../Controller/AtencionController.php";
include_once "/../Controller/PerfilController.php";
include_once "/../Controller/EspecialidadController.php";



/************************  CONSULTAR *******************************/
if ($_GET["consulta"] == "1") {

    $usua = UsuarioController::ConsultarUsuario($_GET["usuario"], $_GET["clave"]);
    echo json_encode($usua);
    die();
}

if ($_GET["consulta"] == "2") {

    $paciente = PacienteController::ConsultarPaciente($_GET["rut"]);
    echo json_encode($paciente);
    die();
}

if ($_GET["consulta"] == "3") {

    $paciente = MedicoController::ConsultarMedico($_GET["rut"]);
    echo json_encode($paciente);
    die();
}


if ($_GET["consulta"] == "4") {

    $atencion = AtencionController::ConsultarAtencion($_GET["idatencion"]);
    echo json_encode($atencion);
    die();
}


if ($_GET["consulta"] == "5") {

    $perfiles = PerfilController::ListarPerfil();
    echo json_encode($perfiles);
    die();
}

if ($_GET["consulta"] == "6") {

    $usua = UsuarioController::ConsultarUsuario($_GET["usuario"]);
    echo json_encode($usua);
    die();
}

if ($_GET["consulta"] == "7") {

    $registro = EspecialidadController::ListarEspecialidad();
    echo json_encode($registro);
    die();
}



if ($_GET["consulta"] == "8") {

    $registro = UsuarioController::ConsultarUsuarioPorRut($_GET["rut"]);
    echo json_encode($registro);

    die();
}



/************************  REGISTRAR  *******************************/


if ($_GET["consulta"] == "10") {

    //registrar
    $paciente = PacienteController::RegistrarPaciente($_GET["rut"], $_GET["nombre_completo"], $_GET["fecha_de_nacimiento"], $_GET["sexo"], $_GET["direccion"], $_GET["telefono"]);
    echo json_encode($paciente);
    die();
}


if ($_GET["consulta"] == "11") {

    //registrar
    $registro = MedicoController::RegistrarMedico($_GET["rut_medico"], $_GET["nombre_medico"], $_GET["fecha_de_contratacion"], $_GET["especialidad"], $_GET["valor_consulta"]);
    echo json_encode($registro);
    die();
}



if ($_GET["consulta"] == "12") {

    //registrar
    $registro = AtencionController::RegistrarAtencion($_GET["fecha"], $_GET["paciente"], $_GET["medico"], $_GET["estado"]);
    echo json_encode($registro);
    die();
}



if ($_GET["consulta"] == "13") {

    //registrar
    
    $password = $_GET["clave_usuario"];
    $passHash = password_hash($password, PASSWORD_BCRYPT);
    $registro = UsuarioController::RegistrarUsuario($_GET["id_perfil"], $_GET["nombre_usuario"], $_GET["login_usuario"], $passHash, $_GET["estado_usuario"]);
    echo json_encode($registro);
    die();
}




/************************  ELIMINAR  *******************************/

if ($_GET["consulta"] == "20") {
    //eliminar
    $paciente = PacienteController::EliminarPaciente($_GET["rut"]);
    echo json_encode($paciente);
    die();
}

if ($_GET["consulta"] == "21") {
    //eliminar
    $medico = MedicoController::EliminarMedico($_GET["rut"]);
    echo json_encode($medico);
    die();
}

if ($_GET["consulta"] == "22") {
    //eliminar
    $atencion = AtencionController::EliminarAtencion($_GET["idAtencion"]);
    echo json_encode($atencion);
    die();
}

if ($_GET["consulta"] == "23") {
    //eliminar
    $idusuario = UsuarioController::EliminarUsuario($_GET["idUsuario"]);
    echo json_encode($idusuario);
    die();
}




/************************  MODIFICAR  *******************************/


if ($_GET["consulta"] == "30") {

    $registro = AtencionController::ActualizarAtencion($_GET["id_atencion"], $_GET["fecha"], $_GET["paciente"], $_GET["medico"], $_GET["estado"]);
    echo json_encode($registro);
    die();
}


?>