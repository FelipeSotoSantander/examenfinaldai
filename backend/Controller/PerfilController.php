<?php

include_once "/../dao/DBConnection.php";
include_once "/../Model/Perfil.php";
include_once "/../dao/PerfilDAO.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PerfilController
 *
 * @author fsoto
 */
class PerfilController {
    //put your code here
    
    
    
    public static function ListarPerfil() {

        $conexion = DBConnection::getConexion();
        $daoPerfil = new PerfilDAO($conexion);


        return $daoPerfil->listarTodos();
    }
    
}
