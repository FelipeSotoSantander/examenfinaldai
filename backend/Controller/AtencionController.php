<?php

include_once "/../dao/DBConnection.php";
include_once "/../Model/Atencion.php";
include_once "/../dao/AtencionDAO.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AtencionController
 *
 * @author fsoto
 */
class AtencionController {

    //put your code here

    public static function ConsultarAtencion($id_atencion) {

        $conexion = DBConnection::getConexion();
        $daoAtencion = new AtencionDAO($conexion);


        return $daoAtencion->ConsultarRegistro($id_atencion);
    }

    public static function EliminarAtencion($id_atencion) {

        $conexion = DBConnection::getConexion();
        $daoAtencion = new AtencionDAO($conexion);


        return $daoAtencion->EliminarRegistro($id_atencion);
    }

    public static function RegistrarAtencion($fecha, $paciente, $medico, $estado) {

        $Atencion = new Atencion();

        $Atencion->setId_atencion(0);
        $Atencion->setFecha($fecha);
        $Atencion->setPaciente($paciente);
        $Atencion->setMedico($medico);
        $Atencion->setEstado($estado);

        $conexion = DBConnection::getConexion();
        $daoAtencion = new AtencionDAO($conexion);


        return $daoAtencion->RegistrarRegistro($Atencion);
    }

    public static function ListarAtencion() {

        $conexion = DBConnection::getConexion();
        $daoAtencion = new AtencionDAO($conexion);


        return $daoAtencion->listarTodos();
    }

    public static function ListarAtencionCompleto() {

        $conexion = DBConnection::getConexion();
        $daoAtencion = new AtencionDAO($conexion);


        return $daoAtencion->listarTodosCompleto();
    }

    public static function ActualizarAtencion($id_atencion, $fecha, $paciente, $medico, $estado) {


        $Atencion = new Atencion();

        $Atencion->setId_atencion($id_atencion);
        $Atencion->setFecha($fecha);
        $Atencion->setPaciente($paciente);
        $Atencion->setMedico($medico);
        $Atencion->setEstado($estado);


        $conexion = DBConnection::getConexion();
        $daoAtencion = new AtencionDAO($conexion);


        return $daoAtencion->ActualizarRegistro($Atencion);
    }

    public static function ConsultarAtencionPorUsuario($id_atencion, $usuario) {

        $conexion = DBConnection::getConexion();
        $daoAtencion = new AtencionDAO($conexion);


        return $daoAtencion->ConsultarRegistroPorUsuario($id_atencion, $usuario);
    }

    public static function ListarAtencionCompletoPorUsuario($usuario) {

        $conexion = DBConnection::getConexion();
        $daoAtencion = new AtencionDAO($conexion);


        return $daoAtencion->listarTodosCompletoPorUsuario($usuario);
    }

}
