<?php


include_once "/../dao/DBConnection.php";
include_once "/../Model/Especialidad.php";
include_once "/../dao/EspecialidadDAO.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EspecialidadController
 *
 * @author fsoto
 */
class EspecialidadController {
    //put your code here
        public static function ListarEspecialidad() {

        $conexion = DBConnection::getConexion();
        $daoEspecialidad = new EspecialidadDAO($conexion);


        return $daoEspecialidad->listarTodos();
    }
    
}
