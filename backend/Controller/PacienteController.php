<?php

include_once "/../dao/DBConnection.php";
include_once "/../Model/Paciente.php";
include_once "/../dao/PacienteDAO.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PacienteController
 *
 * @author fsoto
 */
class PacienteController {

    //put your code here

    public static function ConsultarPaciente($rut) {

        $conexion = DBConnection::getConexion();
        $daoPaciente = new PacienteDAO($conexion);


        return $daoPaciente->ConsultarRegistro($rut);
    }

    public static function EliminarPaciente($rut) {

        $conexion = DBConnection::getConexion();
        $daoPaciente = new PacienteDAO($conexion);


        return $daoPaciente->EliminarRegistro($rut);
    }

    public static function RegistrarPaciente($rut, $nombre_completo, $fecha_de_nacimiento, $sexo, $direccion, $telefono) {

        $Paciente = new Paciente();

        $Paciente->setRut($rut);
        $Paciente->setNombre_completo($nombre_completo);
        $Paciente->setFecha_de_nacimiento($fecha_de_nacimiento);
        $Paciente->setSexo($sexo);
        $Paciente->setDireccion($direccion);
        $Paciente->setTelefono($telefono);

        $conexion = DBConnection::getConexion();
        $daoPaciente = new PacienteDAO($conexion);


        return $daoPaciente->RegistrarRegistro($Paciente);
    }

    public static function ListarPaciente() {

        $conexion = DBConnection::getConexion();
        $daoPaciente = new PacienteDAO($conexion);


        return $daoPaciente->listarTodos();
    }

}
