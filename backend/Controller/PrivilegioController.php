<?php

include_once "/../dao/DBConnection.php";
include_once "/../Model/Privilegio.php";
include_once "/../dao/PrivilegioDAO.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrivilegioController
 *
 * @author fsoto
 */
class PrivilegioController {
    //put your code here
    
        public static function ConsultarUsuario($id_privilegio) {

        $conexion = DBConnection::getConexion();
        $daoPrivilegio = new PrivilegioDAO($conexion);


        return $daoPrivilegio->ConsultarRegistro($id_privilegio);
    }

    
}
