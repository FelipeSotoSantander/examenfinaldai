<?php

include_once "/../dao/DBConnection.php";
include_once "/../Model/Usuario.php";
include_once "/../dao/UsuarioDAO.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuarioController
 *
 * @author fsoto
 */
class UsuarioController {

    //put your code here


    public static function ConsultarUsuario($usuario) {

        $conexion = DBConnection::getConexion();
        $daoUsuario = new UsuarioDAO($conexion);


        return $daoUsuario->ConsultarRegistro($usuario);
    }

    public static function ConsultarUsuarioLogin($usuario, $clave) {

        $conexion = DBConnection::getConexion();
        $daoUsuario = new UsuarioDAO($conexion);


        return $daoUsuario->ConsultarRegistroLogin($usuario, $clave);
    }

    public static function RegistrarUsuario($id_perfil, $nombre_usuario, $login_usuario, $clave_usuario, $estado_usuario, $rut) {

        $Usuario = new Usuario();

        $Usuario->setId_perfil($id_perfil);
        $Usuario->setNombre_usuario($nombre_usuario);
        $Usuario->setLogin_usuario($login_usuario);
        $Usuario->setClave_usuario($clave_usuario);
        $Usuario->setEstado_usuario($estado_usuario);

        if ($rut == "") {
            $Usuario->setRut(NULL);
        } else {
            $Usuario->setRut($rut);
        }


        $conexion = DBConnection::getConexion();
        $daoUsuario = new UsuarioDAO($conexion);


        return $daoUsuario->RegistrarRegistro($Usuario);
    }

    public static function ListarUsuario() {

        $conexion = DBConnection::getConexion();
        $daoUsuario = new UsuarioDAO($conexion);


        return $daoUsuario->listarTodos();
    }

    public static function ListarUsuarioCompleto() {

        $conexion = DBConnection::getConexion();
        $daoUsuario = new UsuarioDAO($conexion);


        return $daoUsuario->listarTodosCompleto();
    }

    public static function EliminarUsuario($idusuario) {

        $conexion = DBConnection::getConexion();
        $daoUsuario = new UsuarioDAO($conexion);


        return $daoUsuario->EliminarRegistro($idusuario);
    }

    public static function ConsultarUsuarioPorRut($rut) {

        $conexion = DBConnection::getConexion();
        $daoUsuario = new UsuarioDAO($conexion);

        return $daoUsuario->ConsultarRegistroPorRut($rut);
    }

}

?>