<?php



include_once "/../dao/DBConnection.php";
include_once "/../Model/Permiso.php";
include_once "/../dao/PermisoDAO.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PermisoController
 *
 * @author fsoto
 */
class PermisoController {
    //put your code here
    
    
        public static function ConsultarPermiso($id_perfil) {

        $conexion = DBConnection::getConexion();
        $daoPermiso = new PermisoDAO($conexion);


        return $daoPermiso->ConsultarRegistro($id_perfil);
    }
    
}
