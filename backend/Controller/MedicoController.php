<?php

include_once "/../dao/DBConnection.php";
include_once "/../Model/Medico.php";
include_once "/../dao/MedicoDAO.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MedicoController
 *
 * @author fsoto
 */
class MedicoController {
    //put your code here
    
    
    public static function ConsultarMedico($rut_medico) {

        $conexion = DBConnection::getConexion();
        $daoMedico = new MedicoDAO($conexion);


        return $daoMedico->ConsultarRegistro($rut_medico);
    }

    public static function EliminarMedico($rut_medico) {

        $conexion = DBConnection::getConexion();
        $daoMedico = new MedicoDAO($conexion);


        return $daoMedico->EliminarRegistro($rut_medico);
    }

    public static function RegistrarMedico($rut_medico, $nombre_medico, $fecha_de_contratacion, $especialidad, $valor_consulta) {

        $Medico = new Medico();

        $Medico->setRut_medico($rut_medico);
        $Medico->setNombre_medico($nombre_medico);
        $Medico->setFecha_de_contratacion($fecha_de_contratacion);
        $Medico->setEspecialidad($especialidad);
        $Medico->setValor_consulta($valor_consulta);

        $conexion = DBConnection::getConexion();
        $daoMedico = new MedicoDAO($conexion);


        return $daoMedico->RegistrarRegistro($Medico);
    }

    public static function ListarMedico() {

        $conexion = DBConnection::getConexion();
        $daoMedico = new MedicoDAO($conexion);


        return $daoMedico->listarTodos();
    }
    
    
        public static function ListarMedicoCompleto() {

        $conexion = DBConnection::getConexion();
        $daoMedico = new MedicoDAO($conexion);

        return $daoMedico->listarTodosCompleto();
    }


    
    
}
