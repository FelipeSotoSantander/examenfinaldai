<?php

include_once "/GenericDAO.php";
include_once "/../Model/Perfil.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuarioDAO
 *
 * @author fsoto
 */
class UsuarioDAO implements GenericDAO {

//put your code here

    private $conexion;

    public function __construct($conexion) {
        $this->conexion = $conexion;
    }

    public function ConsultarRegistro($idRegistro) {

        $sentencia = $this->conexion->prepare("SELECT * FROM usuarios WHERE LOGIN_USUARIO = :LOGIN_USUARIO");  //:email

        $sentencia->bindParam(':LOGIN_USUARIO', $idRegistro);

        $sentencia->execute();
        $Usuario = new Usuario();
        while ($registro = $sentencia->fetch()) {

            $Usuario->setId_usuario($registro["ID_USUARIO"]);
            $Usuario->setId_perfil($registro["ID_PERFIL"]);
            $Usuario->setNombre_usuario($registro["NOMBRE_USUARIO"]);
            $Usuario->setLogin_usuario($registro["LOGIN_USUARIO"]);
            $Usuario->setClave_usuario($registro["CLAVE_USUARIO"]);
            $Usuario->setEstado_usuario($registro["ESTADO_USUARIO"]);
            $Usuario->setRut($registro["RUT"]);
        }

        return $Usuario->ClaseEnArray();
    }

    public function ConsultarRegistroLogin($usuario, $clave) {

        $sentencia = $this->conexion->prepare("SELECT * FROM usuarios WHERE LOGIN_USUARIO = :LOGIN_USUARIO AND CLAVE_USUARIO = :CLAVE_USUARIO");  //:email

        $sentencia->bindParam(':LOGIN_USUARIO', $usuario);
        $sentencia->bindParam(':CLAVE_USUARIO', $clave);

        $sentencia->execute();
        $Usuario = new Usuario();
        while ($registro = $sentencia->fetch()) {

            $Usuario->setId_usuario($registro["ID_USUARIO"]);
            $Usuario->setId_perfil($registro["ID_PERFIL"]);
            $Usuario->setNombre_usuario($registro["NOMBRE_USUARIO"]);
            $Usuario->setLogin_usuario($registro["LOGIN_USUARIO"]);
            $Usuario->setClave_usuario($registro["CLAVE_USUARIO"]);
            $Usuario->setEstado_usuario($registro["ESTADO_USUARIO"]);
            $Usuario->setRut($registro["RUT"]);
        }

        return $Usuario->ClaseEnArray();
    }

    public function EliminarRegistro($idRegistro) {
        $sentencia = $this->conexion->prepare(" delete from usuarios where ID_USUARIO = :ID_USER");
        $sentencia->bindParam(':ID_USER', $idRegistro);
        return $sentencia->execute();
    }

    public function RegistrarRegistro($Reg) {
        $Registro = new Usuario();
        $Registro = $Reg;

        $sentencia = $this->conexion->prepare(" insert into usuarios(ID_PERFIL, NOMBRE_USUARIO, LOGIN_USUARIO, CLAVE_USUARIO, ESTADO_USUARIO, RUT) values(:ID_PERFIL, :NOMBRE_USUARIO, :LOGIN_USUARIO, :CLAVE_USUARIO, :ESTADO_USUARIO, :RUT)");

        $sentencia->bindParam(':ID_PERFIL', $ID_PERFIL);
        $sentencia->bindParam(':NOMBRE_USUARIO', $NOMBRE_USUARIO);
        $sentencia->bindParam(':LOGIN_USUARIO', $LOGIN_USUARIO);
        $sentencia->bindParam(':CLAVE_USUARIO', $CLAVE_USUARIO);
        $sentencia->bindParam(':ESTADO_USUARIO', $ESTADO_USUARIO);
        $sentencia->bindParam(':RUT', $RUT);

        $ID_PERFIL = $Registro->getId_perfil();
        $NOMBRE_USUARIO = $Registro->getNombre_usuario();
        $LOGIN_USUARIO = $Registro->getLogin_usuario();
        $CLAVE_USUARIO = $Registro->getClave_usuario();
        $ESTADO_USUARIO = $Registro->getEstado_usuario();
        $RUT = $Registro->getRut();

        return $sentencia->execute();
    }

    public function listarTodos() {
        
    }

    public function listarTodosCompleto() {


        $listado = array();
        $sentencia = $this->conexion->prepare("select 
                                                        u.ID_USUARIO, 
                                                        u.ID_PERFIL, 
                                                        u.NOMBRE_USUARIO, 
                                                        u.LOGIN_USUARIO, 
                                                        u.CLAVE_USUARIO, 
                                                        u.ESTADO_USUARIO, 
                                                        p.NOMBRE_PERFIL    
                                                from usuarios u inner join perfiles p 
                                                on u.ID_PERFIL = p.ID_PERFIL");

        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {

            $Perfil = new Perfil();

            $Perfil->setId_perfil($registro["ID_PERFIL"]);
            $Perfil->setNombre_perfil($registro["NOMBRE_PERFIL"]);


            $Usuario = new Usuario();

            $Usuario->setId_usuario($registro["ID_USUARIO"]);
            $Usuario->setId_perfil($Perfil->ClaseEnArray());
            $Usuario->setNombre_usuario($registro["NOMBRE_USUARIO"]);
            $Usuario->setLogin_usuario($registro["LOGIN_USUARIO"]);
            $Usuario->setClave_usuario($registro["CLAVE_USUARIO"]);
            $Usuario->setEstado_usuario($registro["ESTADO_USUARIO"]);

            array_push($listado, $Usuario->ClaseEnArray());
        }

        return $listado;
    }

    public function ConsultarRegistroPorRut($rut) {

        $sentencia = $this->conexion->prepare("SELECT * FROM usuarios WHERE RUT is not null and RUT = :RUT");  //:email

        $sentencia->bindParam(':RUT', $rut);
        
        $sentencia->execute();
        $Usuario = new Usuario();
        while ($registro = $sentencia->fetch()) {

            $Usuario->setId_usuario($registro["ID_USUARIO"]);
            $Usuario->setId_perfil($registro["ID_PERFIL"]);
            $Usuario->setNombre_usuario($registro["NOMBRE_USUARIO"]);
            $Usuario->setLogin_usuario($registro["LOGIN_USUARIO"]);
            $Usuario->setClave_usuario($registro["CLAVE_USUARIO"]);
            $Usuario->setEstado_usuario($registro["ESTADO_USUARIO"]);
            $Usuario->setRut($registro["RUT"]);
        }

        return $Usuario->ClaseEnArray();
    }

}
