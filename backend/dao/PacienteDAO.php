<?php

include_once "/GenericDAO.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PacienteDAO
 *
 * @author fsoto
 */
class PacienteDAO implements GenericDAO {

    //put your code here

    private $conexion;

    public function __construct($conexion) {
        $this->conexion = $conexion;
    }

    
    public function ConsultarRegistro($idRegistro) {

        
        $sentencia = $this->conexion->prepare("SELECT * FROM paciente WHERE RUT = :RUT");  //:email

        $sentencia->bindParam(':RUT', $idRegistro);

        $sentencia->execute();
        $Paciente = new Paciente();
        
        while ($registro = $sentencia->fetch()) {
            
            $Paciente->setRut($registro["RUT"]);
            $Paciente->setNombre_completo($registro["NOMBRE_COMPLETO"]);
            $Paciente->setFecha_de_nacimiento($registro["FECHA_DE_NACIMIENTO"]);
            $Paciente->setSexo($registro["SEXO"]);
            $Paciente->setDireccion($registro["DIRECCION"]);
            $Paciente->setTelefono($registro["TELEFONO"]);

        
        }

        return $Paciente->ClaseEnArray();
    }

    public function EliminarRegistro($idRegistro) {
        $sentencia = $this->conexion->prepare(" delete from paciente where RUT = :RUT");
        $sentencia->bindParam(':RUT', $idRegistro);
        return $sentencia->execute();
    }

    public function RegistrarRegistro($Reg) {

        $Registro = new Paciente();
        $Registro = $Reg;
        $sentencia = $this->conexion->prepare(" insert into paciente(RUT, NOMBRE_COMPLETO, FECHA_DE_NACIMIENTO, SEXO, DIRECCION, TELEFONO) values(:RUT, :NOMBRE_COMPLETO, :FECHA_DE_NACIMIENTO, :SEXO, :DIRECCION, :TELEFONO)");

        $sentencia->bindParam(':RUT', $RUT);
        $sentencia->bindParam(':NOMBRE_COMPLETO', $NOMBRE_COMPLETO);
        $sentencia->bindParam(':FECHA_DE_NACIMIENTO', $FECHA_DE_NACIMIENTO);
        $sentencia->bindParam(':SEXO', $SEXO);
        $sentencia->bindParam(':DIRECCION', $DIRECCION);
        $sentencia->bindParam(':TELEFONO', $TELEFONO);


        $RUT = $Registro->getRut();
        $NOMBRE_COMPLETO = $Registro->getNombre_completo();
        $FECHA_DE_NACIMIENTO = $Registro->getFecha_de_nacimiento();
        $SEXO = $Registro->getSexo();
        $DIRECCION = $Registro->getDireccion();
        $TELEFONO = $Registro->getTelefono();

        return $sentencia->execute();
    }

    public function listarTodos() {


        $listado = array();
        $sentencia = $this->conexion->prepare("SELECT * FROM paciente");  //:email

        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {
            $Paciente = new Paciente();
            $Paciente->setRut($registro["RUT"]);
            $Paciente->setNombre_completo($registro["NOMBRE_COMPLETO"]);
            $Paciente->setFecha_de_nacimiento($registro["FECHA_DE_NACIMIENTO"]);
            $Paciente->setSexo($registro["SEXO"]);
            $Paciente->setDireccion($registro["DIRECCION"]);
            $Paciente->setTelefono($registro["TELEFONO"]);

            array_push($listado, $Paciente->ClaseEnArray());
        }

        return $listado;
    }

}
