<?php
include_once "/GenericDAO.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EspacialidadDAO
 *
 * @author fsoto
 */
class EspecialidadDAO implements GenericDAO {
    
    private $conexion;

    public function __construct($conexion) {
        $this->conexion = $conexion;
    }

    public function ConsultarRegistro($idRegistro) {
        
    }

    public function EliminarRegistro($idRegistro) {
        
    }

    public function RegistrarRegistro($Registro) {
        
    }

    public function listarTodos() {
        
        $listado = array();
        $sentencia = $this->conexion->prepare("SELECT * FROM especialidades");

        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {
            $Especialidad = new Especialidad();

            $Especialidad->setEspecialidad($registro["ESPECIALIDAD"]);
            $Especialidad->setNombre_especialidad($registro["NOMBRE_ESPECIALIDAD"]);
            

            array_push($listado, $Especialidad->ClaseEnArray());
        }

        return $listado;
    }

//put your code here
}
