<?php

include_once "/GenericDAO.php";
include_once "/../Model/Especialidad.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MedicoDAO
 *
 * @author fsoto
 */
class MedicoDAO implements GenericDAO {

    //put your code here


    private $conexion;

    public function __construct($conexion) {
        $this->conexion = $conexion;
    }

    public function ConsultarRegistro($idRegistro) {


        $sentencia = $this->conexion->prepare("SELECT * FROM medico WHERE RUT_MEDICO = :RUT_MEDICO");

        $sentencia->bindParam(':RUT_MEDICO', $idRegistro);

        $sentencia->execute();
        $Medico = new Medico();
        while ($registro = $sentencia->fetch()) {
            $Medico->setRut_medico($registro["RUT_MEDICO"]);
            $Medico->setNombre_medico($registro["NOMBRE_MEDICO"]);
            $Medico->setFecha_de_contratacion($registro["FECHA_DE_CONTRATACION"]);
            $Medico->setEspecialidad($registro["ESPECIALIDAD"]);
            $Medico->setValor_consulta($registro["VALOR_CONSULTA"]);
        }

        return $Medico->ClaseEnArray();
    }

    public function EliminarRegistro($idRegistro) {
        $sentencia = $this->conexion->prepare(" delete from medico where RUT_MEDICO = :RUT_MEDICO");
        $sentencia->bindParam(':RUT_MEDICO', $idRegistro);
        return $sentencia->execute();
    }

    public function RegistrarRegistro($Reg) {

        $Registro = new Medico();
        $Registro = $Reg;

        $sentencia = $this->conexion->prepare(" insert into medico(RUT_MEDICO, NOMBRE_MEDICO, FECHA_DE_CONTRATACION, ESPECIALIDAD, VALOR_CONSULTA) values(:RUT_MEDICO, :NOMBRE_MEDICO, :FECHA_DE_CONTRATACION, :ESPECIALIDAD, :VALOR_CONSULTA)");

        $sentencia->bindParam(':RUT_MEDICO', $RUT_MEDICO);
        $sentencia->bindParam(':NOMBRE_MEDICO', $NOMBRE_MEDICO);
        $sentencia->bindParam(':FECHA_DE_CONTRATACION', $FECHA_DE_CONTRATACION);
        $sentencia->bindParam(':ESPECIALIDAD', $ESPECIALIDAD);
        $sentencia->bindParam(':VALOR_CONSULTA', $VALOR_CONSULTA);


        $RUT_MEDICO = $Registro->getRut_medico();
        $NOMBRE_MEDICO = $Registro->getNombre_medico();
        $FECHA_DE_CONTRATACION = $Registro->getFecha_de_contratacion();
        $ESPECIALIDAD = $Registro->getEspecialidad();
        $VALOR_CONSULTA = $Registro->getValor_consulta();

        return $sentencia->execute();
    }

    public function listarTodos() {


        $listado = array();
        $sentencia = $this->conexion->prepare("SELECT * FROM medico");

        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {
            $Medico = new Medico();

            $Medico->setRut_medico($registro["RUT_MEDICO"]);
            $Medico->setNombre_medico($registro["NOMBRE_MEDICO"]);
            $Medico->setFecha_de_contratacion($registro["FECHA_DE_CONTRATACION"]);
            $Medico->setEspecialidad($registro["ESPECIALIDAD"]);
            $Medico->setValor_consulta($registro["VALOR_CONSULTA"]);

            array_push($listado, $Medico->ClaseEnArray());
        }

        return $listado;
    }
    public function listarTodosCompleto() {


        $listado = array();
        $sentencia = $this->conexion->prepare("SELECT m.RUT_MEDICO, m.NOMBRE_MEDICO, m.FECHA_DE_CONTRATACION, m.VALOR_CONSULTA, e.ESPECIALIDAD, e.NOMBRE_ESPECIALIDAD FROM medico m inner join especialidades e on m.especialidad = e.especialidad");

        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {
            
            $Especialidad = new Especialidad();
            
            $Especialidad->setEspecialidad($registro["ESPECIALIDAD"]);
            $Especialidad->setNombre_especialidad($registro["NOMBRE_ESPECIALIDAD"]);
            
            
            $Medico = new Medico();

            $Medico->setRut_medico($registro["RUT_MEDICO"]);
            $Medico->setNombre_medico($registro["NOMBRE_MEDICO"]);
            $Medico->setFecha_de_contratacion($registro["FECHA_DE_CONTRATACION"]);
            $Medico->setEspecialidad($Especialidad->ClaseEnArray());
            $Medico->setValor_consulta($registro["VALOR_CONSULTA"]);

            array_push($listado, $Medico->ClaseEnArray());
        }

        return $listado;
    }

}
