<?php

include_once "/GenericDAO.php";
include_once "/../Model/Paciente.php";
include_once "/../Model/Medico.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AtencionDAO
 *
 * @author fsoto
 */
class AtencionDAO implements GenericDAO {

    //put your code here

    private $conexion;

    public function __construct($conexion) {
        $this->conexion = $conexion;
    }

    public function ConsultarRegistro($idRegistro) {


        $sentencia = $this->conexion->prepare("SELECT * FROM atencion WHERE ID_ATENCION = :ID_ATENCION");

        $sentencia->bindParam(':ID_ATENCION', $idRegistro);

        $sentencia->execute();
        $Atencion = new Atencion();
        while ($registro = $sentencia->fetch()) {
            $Atencion->setId_atencion($registro["ID_ATENCION"]);
            $Atencion->setFecha($registro["FECHA"]);
            $Atencion->setPaciente($registro["RUT"]);
            $Atencion->setMedico($registro["RUT_MEDICO"]);
            $Atencion->setEstado($registro["ESTADO"]);
        }

        return $Atencion->ClaseEnArray();
    }

    public function EliminarRegistro($idRegistro) {
        $sentencia = $this->conexion->prepare(" delete from atencion where ID_ATENCION = :ID_ATE");
        $sentencia->bindParam(':ID_ATE', $idRegistro);
        return $sentencia->execute();
    }

    public function RegistrarRegistro($Reg) {

        $Registro = new Atencion();
        $Registro = $Reg;

        $sentencia = $this->conexion->prepare(" insert into atencion(FECHA, RUT, RUT_MEDICO, ESTADO) values(:FECHA, :PACIENTE, :MEDICO, :ESTADO)");

        $sentencia->bindParam(':FECHA', $FECHA);
        $sentencia->bindParam(':PACIENTE', $PACIENTE);
        $sentencia->bindParam(':MEDICO', $MEDICO);
        $sentencia->bindParam(':ESTADO', $ESTADO);

        $FECHA = $Registro->getFecha();
        $PACIENTE = $Registro->getPaciente();
        $MEDICO = $Registro->getMedico();
        $ESTADO = $Registro->getEstado();




        return $sentencia->execute();
    }

    public function listarTodos() {


        $listado = array();
        $sentencia = $this->conexion->prepare("SELECT * FROM atencion");

        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {
            $Atencion = new Atencion();

            $Atencion->setId_atencion($registro["ID_ATENCION"]);
            $Atencion->setFecha($registro["FECHA"]);
            $Atencion->setPaciente($registro["RUT"]);
            $Atencion->setMedico($registro["RUT_MEDICO"]);
            $Atencion->setEstado($registro["ESTADO"]);

            array_push($listado, $Atencion->ClaseEnArray());
        }

        return $listado;
    }

    public function listarTodosCompleto() {


        $listado = array();
        $sentencia = $this->conexion->prepare("select 
                                                a.ID_ATENCION, a.FECHA, a.RUT, a.RUT_MEDICO, a.ESTADO,
                                                p.NOMBRE_COMPLETO, p.FECHA_DE_NACIMIENTO, p.SEXO, p.DIRECCION, p.TELEFONO,
                                                m.NOMBRE_MEDICO, m.FECHA_DE_CONTRATACION, m.ESPECIALIDAD, m.VALOR_CONSULTA 
                                                from atencion a 
                                                inner join paciente p on a.RUT = p.RUT 
                                                inner join medico m on a.RUT_MEDICO = m.RUT_MEDICO ;
                                            ");


        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {

            $Paciente = new Paciente();

            $Paciente->setRut($registro["RUT"]);
            $Paciente->setNombre_completo($registro["NOMBRE_COMPLETO"]);
            $Paciente->setFecha_de_nacimiento($registro["FECHA_DE_NACIMIENTO"]);
            $Paciente->setSexo($registro["SEXO"]);
            $Paciente->setDireccion($registro["DIRECCION"]);
            $Paciente->setTelefono($registro["TELEFONO"]);

            $Medico = new Medico();

            $Medico->setRut_medico($registro["RUT_MEDICO"]);
            $Medico->setNombre_medico($registro["NOMBRE_MEDICO"]);
            $Medico->setFecha_de_contratacion($registro["FECHA_DE_CONTRATACION"]);
            $Medico->setEspecialidad($registro["ESPECIALIDAD"]);
            $Medico->setValor_consulta($registro["VALOR_CONSULTA"]);


            $Atencion = new Atencion();


            $Atencion->setId_atencion($registro["ID_ATENCION"]);
            $Atencion->setFecha($registro["FECHA"]);
            $Atencion->setPaciente($Paciente->ClaseEnArray());
            $Atencion->setMedico($Medico->ClaseEnArray());
            $Atencion->setEstado($registro["ESTADO"]);

            array_push($listado, $Atencion->ClaseEnArray());
        }

        return $listado;
    }

    public function ActualizarRegistro($Reg) {

        $Registro = new Atencion();
        $Registro = $Reg;

        $sentencia = $this->conexion->prepare(" update atencion SET FECHA = :FECHA, RUT = :PACIENTE, RUT_MEDICO = :MEDICO, ESTADO = :ESTADO WHERE ID_ATENCION = :ID_ATENCION");


        $ID_ATENCION = $Registro->getId_atencion();
        $FECHA = $Registro->getFecha();
        $PACIENTE = $Registro->getPaciente();
        $MEDICO = $Registro->getMedico();
        $ESTADO = $Registro->getEstado();

        $sentencia->bindParam(':ID_ATENCION', $ID_ATENCION);
        $sentencia->bindParam(':FECHA', $FECHA);
        $sentencia->bindParam(':PACIENTE', $PACIENTE);
        $sentencia->bindParam(':MEDICO', $MEDICO);
        $sentencia->bindParam(':ESTADO', $ESTADO);




        return $sentencia->execute();
    }

    public function ConsultarRegistroPorUsuario($idRegistro, $usuario) {


        $sentencia = $this->conexion->prepare("SELECT * FROM atencion WHERE ID_ATENCION = :ID_ATENCION AND RUT = :RUT");

        $sentencia->bindParam(':ID_ATENCION', $idRegistro);
        $sentencia->bindParam(':RUT', $usuario);

        $sentencia->execute();
        $Atencion = new Atencion();
        while ($registro = $sentencia->fetch()) {
            $Atencion->setId_atencion($registro["ID_ATENCION"]);
            $Atencion->setFecha($registro["FECHA"]);
            $Atencion->setPaciente($registro["RUT"]);
            $Atencion->setMedico($registro["RUT_MEDICO"]);
            $Atencion->setEstado($registro["ESTADO"]);
        }

        return $Atencion->ClaseEnArray();
    }
    
    
    
      public function listarTodosCompletoPorUsuario($usuario) {


        $listado = array();
        $sentencia = $this->conexion->prepare("select 
                                                a.ID_ATENCION, a.FECHA, a.RUT, a.RUT_MEDICO, a.ESTADO,
                                                p.NOMBRE_COMPLETO, p.FECHA_DE_NACIMIENTO, p.SEXO, p.DIRECCION, p.TELEFONO,
                                                m.NOMBRE_MEDICO, m.FECHA_DE_CONTRATACION, m.ESPECIALIDAD, m.VALOR_CONSULTA 
                                                from atencion a 
                                                inner join paciente p on a.RUT = p.RUT 
                                                inner join medico m on a.RUT_MEDICO = m.RUT_MEDICO 
                                                where p.RUT = :RUT ;
                                            ");

        $sentencia->bindParam(':RUT', $usuario);
        

        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {

            $Paciente = new Paciente();

            $Paciente->setRut($registro["RUT"]);
            $Paciente->setNombre_completo($registro["NOMBRE_COMPLETO"]);
            $Paciente->setFecha_de_nacimiento($registro["FECHA_DE_NACIMIENTO"]);
            $Paciente->setSexo($registro["SEXO"]);
            $Paciente->setDireccion($registro["DIRECCION"]);
            $Paciente->setTelefono($registro["TELEFONO"]);

            $Medico = new Medico();

            $Medico->setRut_medico($registro["RUT_MEDICO"]);
            $Medico->setNombre_medico($registro["NOMBRE_MEDICO"]);
            $Medico->setFecha_de_contratacion($registro["FECHA_DE_CONTRATACION"]);
            $Medico->setEspecialidad($registro["ESPECIALIDAD"]);
            $Medico->setValor_consulta($registro["VALOR_CONSULTA"]);


            $Atencion = new Atencion();


            $Atencion->setId_atencion($registro["ID_ATENCION"]);
            $Atencion->setFecha($registro["FECHA"]);
            $Atencion->setPaciente($Paciente->ClaseEnArray());
            $Atencion->setMedico($Medico->ClaseEnArray());
            $Atencion->setEstado($registro["ESTADO"]);

            array_push($listado, $Atencion->ClaseEnArray());
        }

        return $listado;
    }


}
