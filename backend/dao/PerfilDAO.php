<?php

include_once "/GenericDAO.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PerfilDAO
 *
 * @author fsoto
 */
class PerfilDAO implements GenericDAO {

    private $conexion;

    public function __construct($conexion) {
        $this->conexion = $conexion;
    }

    public function ConsultarRegistro($idRegistro) {

        
    }

    public function EliminarRegistro($idRegistro) {
        
    }

    public function RegistrarRegistro($Registro) {
        
    }

    public function listarTodos() {
        
        $listado = array();
        $sentencia = $this->conexion->prepare("SELECT * FROM perfiles");

        $sentencia->execute();

        while ($registro = $sentencia->fetch()) {
            $Perfil = new Perfil();

            $Perfil->setId_perfil($registro["ID_PERFIL"]);
            $Perfil->setNombre_perfil($registro["NOMBRE_PERFIL"]);

            array_push($listado, $Perfil->ClaseEnArray());
        }

        return $listado;
    }

//put your code here
}
