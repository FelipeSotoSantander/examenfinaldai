<?php

include_once "/GenericDAO.php";

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrivilegioDAO
 *
 * @author fsoto
 */
class PrivilegioDAO implements GenericDAO {

    private $conexion;

    public function __construct($conexion) {
        $this->conexion = $conexion;
    }

    public function ConsultarRegistro($idRegistro) {

        $sentencia = $this->conexion->prepare("SELECT * FROM privilegios WHERE ID_PRIVILEGIO = :ID_PRIVILEGIO");  //:email

        $sentencia->bindParam(':ID_PRIVILEGIO', $idRegistro);

        $sentencia->execute();
        $Privilegio = new Privilegio();
        while ($registro = $sentencia->fetch()) {
            
            $Privilegio->setId_privilegio($registro["ID_PRIVILEGIO"]);
            $Privilegio->setNombre_privilegio($registro["NOMBRE_PRIVILEGIO"]);
            $Privilegio->setUrl_privilegio($registro["URL_PRIVILEGIO"]);

        }

        return $Privilegio->ClaseEnArray();
    }

    public function EliminarRegistro($idRegistro) {
        
    }

    public function RegistrarRegistro($Registro) {
        
    }

    public function listarTodos() {
        
    }

//put your code here
}
