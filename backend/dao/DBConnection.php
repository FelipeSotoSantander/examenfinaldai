<?php

class DBConnection {

    const HOST = "localhost";
    const DBNAME = "DAI5501_ExamenFinal";
    const PORT = "3306";
    const USER = "root";
    const PASS = "";

    public static function getConexion() {
        $dsn = "mysql:host=" . self::HOST . ";dbname=" . self::DBNAME . ";port=" . self::PORT . ";charset=utf8";

        try {
            $dbConexion = new PDO($dsn, self::USER, self::PASS);
            return $dbConexion;
        } catch (PDOException $exception) {
            switch ($exception->getCode()) {
                case 2002:
                    echo '<div class="error">No se pudo establecer la conexión con la base de datos, revise que &eacute;sta se encuentre en ejecuci&oacute;n.</div>';
                    exit;
                case 1045:
                    echo '<div class="error">No se pudo conectar a la base de datos, revise las credenciales configuradas</div>';
                    exit;
                case 1049: // La base de datos no existe.                        
                    $dbConexion = self::crearBaseDatos();
                    return $dbConexion;
                default:
                    echo '<div class="error">' . $exception->getMessage() . '</div>';
                    break;
            }
        }
    }

    private static function crearBaseDatos() {

        echo '<div class="warning">Base de datos no encontrada, se crear&aacute;...</div>';

        try {
            $dsn = "mysql:host=" . self::HOST . ";port=" . self::PORT . ";charset=utf8";
            $mysqlConexion = new PDO($dsn, self::USER, self::PASS);
            $mysqlConexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $mysqlConexion->exec("CREATE DATABASE " . self::DBNAME);
            $mysqlConexion->exec("USE " . self::DBNAME);

            $mysqlConexion->exec("
    CREATE TABLE IF NOT EXISTS `perfiles` (
    `ID_PERFIL` int(8) NOT NULL,
    `NOMBRE_PERFIL` varchar(50) NOT NULL,
     PRIMARY KEY (`ID_PERFIL`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

            $mysqlConexion->exec("
    INSERT INTO `perfiles` (`ID_PERFIL`, `NOMBRE_PERFIL`) VALUES
    (1, 'Director'),
    (2, 'Administrador'),
    (3, 'Secretaria'),
    (4, 'Paciente')");


            $mysqlConexion->exec("
    CREATE TABLE IF NOT EXISTS `privilegios` (
    `ID_PRIVILEGIO` int(8) NOT NULL,
    `NOMBRE_PRIVILEGIO` varchar(50) NOT NULL,
    `URL_PRIVILEGIO` varchar(50) NOT NULL,
     PRIMARY KEY (`ID_PRIVILEGIO`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

            $mysqlConexion->exec("
    INSERT INTO `privilegios` (`ID_PRIVILEGIO`, `NOMBRE_PRIVILEGIO` , `URL_PRIVILEGIO`) VALUES
    (50, 'Listar Paciente', 'ListarPaciente.php'),
    (51, 'Listar Medico', 'ListarMedico.php'),
    (52, 'Listar Atenciones', 'ListarAtenciones.php'),
    (53, 'Listar Usuarios', 'ListarUsuarios.php'),
    
    (54, 'Consultar Paciente', 'ConsultarPaciente.php'),
    (55, 'Consultar Medico', 'ConsultarMedico.php'),
    (56, 'Consultar Atenciones', 'ConsultarAtenciones.php'),
    (57, 'Consultar Usuarios', 'ConsultarUsuarios.php'),
    
    (58, 'Registrar Paciente', 'RegistrarPaciente.php'),
    (59, 'Registrar Medico', 'RegistrarMedico.php'),
    (60, 'Registrar Atenciones', 'RegistrarAtenciones.php'),
    (61, 'Registrar Usuarios', 'RegistrarUsuarios.php'),
    
    (62, 'Eliminar Paciente', 'EliminarPaciente.php'),
    (63, 'Eliminar Medico', 'EliminarMedico.php'),
    (64, 'Eliminar Atenciones', 'EliminarAtenciones.php'),
    (65, 'Eliminar Usuarios', 'EliminarUsuarios.php'),
    
    (66, 'Actualizar Atenciones', 'ActualizarAtenciones.php'),
    
    (67, 'Listar Atenciones Por Paciente', 'ListarAtencionesPorPaciente.php'),
    (68, 'Consultar Atenciones Por Paciente', 'ConsultarAtencionesPorPaciente.php')
   
    ");






            $mysqlConexion->exec("
    CREATE TABLE IF NOT EXISTS `permisos` (
    `ID_PERMISO` int(8) NOT NULL AUTO_INCREMENT,
    `ID_PERFIL` int(8) NOT NULL,
    `ID_PRIVILEGIO` int(8) NOT NULL,
     PRIMARY KEY (`ID_PERMISO`),
     KEY `ID_PERFIL` (`ID_PERFIL`),
     KEY `ID_PRIVILEGIO` (`ID_PRIVILEGIO`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");


            $mysqlConexion->exec("
    CREATE TABLE IF NOT EXISTS `especialidades` (
    `ESPECIALIDAD` int(8) NOT NULL AUTO_INCREMENT,
    `NOMBRE_ESPECIALIDAD` varchar(150) NOT NULL,
     PRIMARY KEY (`ESPECIALIDAD`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");






            $mysqlConexion->exec("
    CREATE TABLE IF NOT EXISTS `usuarios` (
    `ID_USUARIO` int(8) NOT NULL AUTO_INCREMENT,
    `ID_PERFIL` int(3) NOT NULL,
    `NOMBRE_USUARIO` varchar(50) NOT NULL,
    `LOGIN_USUARIO` varchar(50) NOT NULL,
    `CLAVE_USUARIO` varchar(200) NOT NULL,
    `ESTADO_USUARIO` int(3) NOT NULL,
    `RUT` int(8) DEFAULT NULL,
     PRIMARY KEY (`ID_USUARIO`),
     KEY `ID_PERFIL` (`ID_PERFIL`),
     KEY `RUT` (`RUT`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");


            $mysqlConexion->exec("
    CREATE TABLE IF NOT EXISTS `paciente` (
    `RUT` int(8) NOT NULL,
    `NOMBRE_COMPLETO` varchar(50) NOT NULL,
    `FECHA_DE_NACIMIENTO` DATE NOT NULL,
    `SEXO` varchar(10) NOT NULL,
    `DIRECCION` varchar(50) NOT NULL,
    `TELEFONO` varchar(30) NOT NULL,
     PRIMARY KEY (`RUT`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");


            $mysqlConexion->exec("
    CREATE TABLE IF NOT EXISTS `medico` (
        `RUT_MEDICO` int(8) NOT NULL,
        `NOMBRE_MEDICO` varchar(50) NOT NULL,
        `FECHA_DE_CONTRATACION` DATE NOT NULL,
        `ESPECIALIDAD` int(8) NOT NULL,
        `VALOR_CONSULTA` int(20) NOT NULL,
        PRIMARY KEY (`RUT_MEDICO`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");




            $mysqlConexion->exec("
    CREATE TABLE IF NOT EXISTS `atencion` (
        `ID_ATENCION` int(8) NOT NULL AUTO_INCREMENT,
        `FECHA` DATE NOT NULL,
        `RUT` int(8) NOT NULL,
        `RUT_MEDICO` int(8) NOT NULL,
        `ESTADO` varchar(20) NOT NULL,
        PRIMARY KEY (`ID_ATENCION`),
        KEY `RUT` (`RUT`),
        KEY `RUT_MEDICO` (`RUT_MEDICO`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");


            $mysqlConexion->exec("
            ALTER TABLE `usuarios`
            ADD CONSTRAINT `usuarios_fk_1` FOREIGN KEY (`RUT`) REFERENCES `paciente` (`RUT`) ON UPDATE CASCADE");


            $mysqlConexion->exec("
            ALTER TABLE `atencion`
            ADD CONSTRAINT `atencion_fk_1` FOREIGN KEY (`RUT`) REFERENCES `paciente` (`RUT`) ON UPDATE CASCADE");

            $mysqlConexion->exec("
            ALTER TABLE `atencion`
            ADD CONSTRAINT `atencion_fk_2` FOREIGN KEY (`RUT_MEDICO`) REFERENCES `medico` (`RUT_MEDICO`) ON UPDATE CASCADE");


            $mysqlConexion->exec("
            ALTER TABLE `permisos`
            ADD CONSTRAINT `permmisos_fk_1` FOREIGN KEY (`ID_PERFIL`) REFERENCES `perfiles` (`ID_PERFIL`) ON UPDATE CASCADE");


            $mysqlConexion->exec("
            ALTER TABLE `permisos`
            ADD CONSTRAINT `permmisos_fk_2` FOREIGN KEY (`ID_PRIVILEGIO`) REFERENCES `privilegios` (`ID_PRIVILEGIO`) ON UPDATE CASCADE");



            $mysqlConexion->exec("
            ALTER TABLE `medico`
            ADD CONSTRAINT `medico_fk_1` FOREIGN KEY (`ESPECIALIDAD`) REFERENCES `especialidades` (`ESPECIALIDAD`) ON UPDATE CASCADE");



            $mysqlConexion->exec("
            INSERT INTO `permisos` (`ID_PERFIL`, `ID_PRIVILEGIO`) VALUES
            (1, 50),
            (1, 51),
            (1, 52),
            (1, 53),
            (1, 54),
            (1, 55),
            (1, 56),
            (1, 57),
            (1, 58),
            (1, 59),
            (1, 60),
            (1, 61),
            (1, 62),
            (1, 63),
            (1, 64),
            (1, 65),
            (1, 66),
            (1, 67),
            (1, 68)");




            $mysqlConexion->exec("
            INSERT INTO `especialidades` (`NOMBRE_ESPECIALIDAD`) VALUES
            ('Especialidades clínicas - Alergología'),
('Especialidades clínicas - Anestesiología y reanimación'),
('Especialidades clínicas - Cardiología'),
('Especialidades clínicas - Gastroenterología'),
('Especialidades clínicas - Endocrinología'),
('Especialidades clínicas - Geriatría'),
('Especialidades clínicas - Hematología y hemoterapia'),
('Especialidades clínicas - Infectología'),
('Especialidades clínicas - Medicina aeroespacial'),
('Especialidades clínicas - Medicina del deporte'),
('Especialidades clínicas - Medicina del trabajo'),
('Especialidades clínicas - Medicina de urgencias'),
('Especialidades clínicas - Medicina familiar y comunitaria'),
('Especialidades clínicas - Medicina física y rehabilitación'),
('Especialidades clínicas - Medicina intensiva'),
('Especialidades clínicas - Medicina interna'),
('Especialidades clínicas - Medicina legal y forense'),
('Especialidades clínicas - Medicina preventiva y salud pública'),
('Especialidades clínicas - Nefrología'),
('Especialidades clínicas - Neumología'),
('Especialidades clínicas - Neurología'),
('Especialidades clínicas - Nutriología'),
('Especialidades clínicas - Oftalmología'),
('Especialidades clínicas - Oncología médica'),
('Especialidades clínicas - Oncología radioterápica'),
('Especialidades clínicas - Pediatría'),
('Especialidades clínicas - Psiquiatría'),
('Especialidades clínicas - Rehabilitación'),
('Especialidades clínicas - Reumatología'),
('Especialidades clínicas - Toxicología'),
('Especialidades clínicas - Urología'),
('Especialidades quirúrgicas - Cirugía cardiovascular'),
('Especialidades quirúrgicas - Cirugía general y del aparato digestivo'),
('Especialidades quirúrgicas - Cirugía oral y maxilofacial'),
('Especialidades quirúrgicas - Cirugía ortopédica y traumatología'),
('Especialidades quirúrgicas - Cirugía pediátrica'),
('Especialidades quirúrgicas - Cirugía plástica, estética y reparadora'),
('Especialidades quirúrgicas - Cirugía torácica'),
('Especialidades quirúrgicas - Neurocirugía'),
('Especialidades quirúrgicas - Proctología'),
('Especialidades médico-quirúrgicas - Angiología y cirugía vascular'),
('Especialidades médico-quirúrgicas - Dermatología médico-quirúrgica y venereología'),
('Especialidades médico-quirúrgicas - Estomatología'),
('Especialidades médico-quirúrgicas - Ginecología y obstetricia o tocología'),
('Especialidades médico-quirúrgicas - Oftalmología'),
('Especialidades médico-quirúrgicas - Otorrinolaringología'),
('Especialidades médico-quirúrgicas - Urología'),
('Especialidades médico-quirúrgicas - Traumatología'),
('Especialidades de laboratorio o diagnósticas - Análisis clínicos'),
('Especialidades de laboratorio o diagnósticas - Anatomía patológica'),
('Especialidades de laboratorio o diagnósticas - Bioquímica clínica'),
('Especialidades de laboratorio o diagnósticas - Farmacología clínica'),
('Especialidades de laboratorio o diagnósticas - Genética médica'),
('Especialidades de laboratorio o diagnósticas - Inmunología'),
('Especialidades de laboratorio o diagnósticas - Medicina nuclear'),
('Especialidades de laboratorio o diagnósticas - Microbiología y parasitología'),
('Especialidades de laboratorio o diagnósticas - Neurofisiología clínica'),
('Especialidades de laboratorio o diagnósticas - Radiodiagnóstico o radiología')");


            $mysqlConexion->exec("
            INSERT INTO `usuarios` (`ID_PERFIL`, `NOMBRE_USUARIO`, `LOGIN_USUARIO`, `CLAVE_USUARIO`, `ESTADO_USUARIO`) VALUES
            (1, 'Felipe Soto','fsoto','fsoto',1)");


            $mysqlConexion->exec("
            INSERT INTO `paciente` (`RUT`, `NOMBRE_COMPLETO`, `FECHA_DE_NACIMIENTO`, `SEXO`, `DIRECCION`, `TELEFONO`) VALUES
            ('1','Paciente1',now(),'Masculino','direccion paciente 1','50145232')");


            $mysqlConexion->exec("
            INSERT INTO `medico` (`RUT_MEDICO`, `NOMBRE_MEDICO`, `FECHA_DE_CONTRATACION`, `ESPECIALIDAD`, `VALOR_CONSULTA`) VALUES
            ('2','medico1',now(),1,'40000')");



            $mysqlConexion->exec("
            INSERT INTO `atencion` (`FECHA`, `RUT`, `RUT_MEDICO`, `ESTADO`) VALUES
            (now(),1,2,'agendada')");




//            
            return $mysqlConexion;
        } catch (Exception $e) {
            echo $e->getMessage();
            die($e->getCode());
        }
    }

}
