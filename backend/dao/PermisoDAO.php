<?php


include_once "/GenericDAO.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PermisoDAO
 *
 * @author fsoto
 */
class PermisoDAO implements GenericDAO {
    //put your code here
    
    
    private $conexion;

    public function __construct($conexion) {
        $this->conexion = $conexion;
    }

    public function ConsultarRegistro($id_perfil) {

        $listado = array();
        
//        $sentencia = $this->conexion->prepare("SELECT * FROM permisos WHERE ID_PERFIL = :ID_PERFIL");
        $sentencia = $this->conexion->prepare("select a.ID_PERMISO, a.ID_PERFIL, a.ID_PRIVILEGIO, b.NOMBRE_PRIVILEGIO, b.URL_PRIVILEGIO from permisos a inner join privilegios b on a.ID_PRIVILEGIO = b.ID_PRIVILEGIO where a.ID_PERFIL = :ID_PERFIL");

        $sentencia->bindParam(':ID_PERFIL', $id_perfil);

        $sentencia->execute();
        
        while ($registro = $sentencia->fetch()) {
            
            $Privilegio = new Privilegio();

            $Privilegio->setId_privilegio($registro["ID_PRIVILEGIO"]);
            $Privilegio->setNombre_privilegio($registro["NOMBRE_PRIVILEGIO"]);
            $Privilegio->setUrl_privilegio($registro["URL_PRIVILEGIO"]);            
            
            $Permiso = new Permiso();
            
            $Permiso->setId_permiso($registro["ID_PERMISO"]);
            $Permiso->setId_perfil($registro["ID_PERFIL"]);
            $Permiso->setId_privilegio($registro["ID_PRIVILEGIO"]);
            $Permiso->setPrivilegio($Privilegio->ClaseEnArray());
            
            
            array_push($listado, $Permiso->ClaseEnArray());
        }

        return $listado;
    }

    public function EliminarRegistro($idRegistro) {
        
    }

    public function RegistrarRegistro($Registro) {
        
    }

    public function listarTodos() {
        
    }

}
