<?php
session_start();
?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/RegistrarMedico.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Registrar Médico</h1>
            </header>
            <div id="contenido">
                <div id="cargandoAjax">
                    <img src="img/ajax-loader.gif" alt="cargando..."/>
                </div>
                <form action="RegistrarMedico.php" method="POST" name="frmRegistrarMedico" id="frmRegistrarMedico">
                    <fieldset>
                        <legend>Medico</legend>
                        <div class="campoFormulario">
                            Rut Médico:
                            <input id="txtrut_medico" name="txtrut_medico" type="text" placeholder="xx.xxx.xxx-x" required/>
                            Nombre Completo:
                            <input id="txtnombre_medico" name="txtnombre_medico" type="text" required/>
                            Fecha de Contratación:
                            <input id="txtfecha_de_contratacion" name="txtfecha_de_contratacion" type="date" required/>
                            Especialidad:
                            <div class="styled-select slate" required>
                                <select id="txtespecialidad" name="txtespecialidad" >
                                </select>
                            </div>
                            Valor Consulta:
                            <input id="txtvalor_consulta" name="txtvalor_consulta" type="number" required/>
                        </div>
                        <br/>
                        <div class="botonera">

                            <input type="reset" value="Limpiar" name="limpiar" />
                            <input type="submit" value="Registrar" name="registrarPaciente" />
                            <input type="button" value="Volver" name="volver" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
