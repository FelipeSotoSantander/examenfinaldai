
<?php
session_start();

include_once "/../backend/Controller/UsuarioController.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["usuario"]) && isset($_POST["clave"])) {        
        
        $user = UsuarioController::ConsultarUsuario($_POST["usuario"]);
        if ($user!=null) {
            $claveUsuario = $user["clave_usuario"];            
            if (password_verify($_POST["clave"], $claveUsuario)) {
                $usua = UsuarioController::ConsultarUsuarioLogin($_POST["usuario"], $claveUsuario);
        
                if ($usua["id_usuario"]!=null) {

                    $_SESSION['usuario'] = $usua;            
                    header("location: Menu.php");
                    return;
                }
            }            
        }else{
            echo 'Error con los datos ingresados';
        }
    }
}
?>

<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Login</h1>
            </header>
            <div id="contenidoLogin">
                <form action="Login.php" method="POST" name="formularioLogin" >
                    <fieldset>
                        <legend>Login del Sistema</legend>

                        <div class="campoFormulario">
                            Usuario <input type="text" name="usuario" placeholder="Ingrese su Usuario"  required />
                        </div>
                        <div class="campoFormulario">
                            Clave <input type="password" name="clave" placeholder="Ingrese su Clave" required />
                        </div>
                        <div class="botonera">
                            <input type="reset" name="limpiar" />
                            <input type="submit" value="Enviar" name="enviar" />
                        </div>  
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
