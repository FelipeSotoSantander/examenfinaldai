<?php
session_start();
?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/ConsultarAtenciones.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Consultar Atención</h1>
            </header>
            <div id="contenido">
                <div id="cargandoAjax">
                    <img src="img/ajax-loader.gif" alt="cargando..."/>
                </div>
                <form action="Login.php" method="POST" name="formulario" >
                    <fieldset>
                        <legend>Atencion</legend>
                        <div class="campoFormulario">
                            Número de Atención: 
                            <input id="txtid_atencion" name="txtid_atencion" type="number" required/>
                            Fecha: 
                            <input id="txtfecha" name="txtfecha" type="date" readonly/>
                            Rut Paciente: 
                            <input id="txtpaciente" name="txtpaciente" placeholder="xx.xxx.xxx-x" type="text" readonly/>
                            Rut Médico: 
                            <input id="txtmedico" name="txtmedico" placeholder="xx.xxx.xxx-x" type="text" readonly/>
                            Estado: 
                            <div class="styled-select slate">
                                <select id="txtestado" name="txtestado" disabled="true">
                                    <option value="">Seleccione</option>
                                    <option value="agendada">Agendada</option>
                                    <option value="confirmada">Confirmada</option>
                                    <option value="anulada">Anulada</option>
                                    <option value="perdida">Perdida</option>
                                    <option value="realizada">Realizada</option>
                                </select>
                            </div>
                        </div>
                        <br/>
                        <div class="botonera">

                            <input type="reset" value="Limpiar" name="limpiar" />
                            <input type="button" value="Volver" name="volver" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
