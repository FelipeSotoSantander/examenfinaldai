<?php
session_start();
?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/RegistrarAtenciones.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Registrar Atenciones</h1>
            </header>
            <div id="contenido">
                <div id="cargandoAjax">
                    <img src="img/ajax-loader.gif" alt="cargando..."/>
                </div>
                <form action="RegistrarAtenciones.php" method="POST" name="frmRegistrarAtenciones" id="frmRegistrarAtenciones">
                    <fieldset>
                        <legend>Atenciones</legend>
                        <div class="campoFormulario">
                            Fecha: 
                            <input id="txtfecha" name="txtfecha" type="date" required/>
                            Paciente: 
                            <input id="txtpaciente" name="txtpaciente" type="text" required/>
                            Médico: 
                            <input id="txtmedico" name="txtmedico" type="text" required/>
                            Estado: 
                            <div class="styled-select slate">
                                <select id="txtestado" name="txtestado" required>
                                    <option value="">Seleccione</option>
                                    <option value="agendada">Agendada</option>
                                    <option value="confirmada">Confirmada</option>
                                    <option value="anulada">Anulada</option>
                                    <option value="perdida">Perdida</option>
                                    <option value="realizada">Realizada</option>
                                </select>
                            </div>


                        </div>
                        <br/>
                        <div class="botonera">

                            <input type="reset" value="Limpiar" name="limpiar" />
                            <input type="submit" value="Registrar" name="registrarPaciente" />
                            <input type="button" value="Volver" name="volver" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
