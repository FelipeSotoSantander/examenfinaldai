<?php
session_start();
?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/ConsultarMedico.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Consultar Médico</h1>
            </header>
            <div id="contenido">
                <div id="cargandoAjax">
                    <img src="img/ajax-loader.gif" alt="cargando..."/>
                </div>
                <form action="Login.php" method="POST" name="formulario" >
                    <fieldset>
                        <legend>Médico</legend>
                        <div class="campoFormulario">
                            Rut Médico:
                            <input id="txtrut_medico" name="txtrut_medico" placeholder="xx.xxx.xxx-x" type="text"/>
                            Nombre Completo:
                            <input id="txtnombre_medico" name="txtnombre_medico" type="text" readonly/>
                            Fecha de Contratación:
                            <input id="txtfecha_de_contratacion" name="txtfecha_de_contratacion" type="date" readonly/>
                            Especialidad:
                            <div class="styled-select slate" >
                                <select id="txtespecialidad" name="txtespecialidad" disabled="true">
                                </select>
                            </div>


                            Valor Consulta:
                            <input id="txtvalor_consulta" name="txtvalor_consulta" type="number" readonly/>

                        </div>
                        <br/>
                        <div class="botonera">

                            <input type="reset" value="Limpiar" name="limpiar" />                            
                            <input type="button" value="Volver" name="volver" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
