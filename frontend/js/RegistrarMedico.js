jQuery(document).ready(function () {

    $.ajaxSetup({
        async: false
    });
    $("input[name='txtrut_medico']").Rut({
        format_on: 'keyup'
    });



    /************************   Cargar Especialidades   ************************/

    jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
            {"consulta": 7},
            function (especialidades) {

                console.log(especialidades);

                jQuery("select[name='txtespecialidad'] option").remove();
                jQuery("select[name='txtespecialidad']").append("<option value=\"\">Seleccione Especialidad</option>");

                jQuery.each(especialidades, function (indice, especialidad) {
                    jQuery("select[name='txtespecialidad']").append("<option value=\"" + especialidad.especialidad + "\">" + especialidad.nombre_especialidad + "</option>");
                });


            });

    /************************   Registrar Medico ************************/

    jQuery("form[name='frmRegistrarMedico']").submit(function (evt) {

        vrRut = $("input[name='txtrut_medico']").val()
        if (vrRut !== "") {
            valido = jQuery.Rut.validar(vrRut);
            if (valido) {
//                alert("aqui");
                jQuery("#cargandoAjax").css("visibility", "visible");
                //pasa el valor del campo rut a valorRut
                valorRut = vrRut;
                //le quita los puntos y guion
                rutLimpio = $.Rut.quitarFormato(valorRut);
                //le quita el dv
                rutLimpio = rutLimpio.substring(0, rutLimpio.length - 1)
                //asigna el rutLimpio al campo rut
//                jQuery("input[name='rut']").val(rutLimpio);
                //pasa a buscar el JSON con los datos
                vrMedico = null;
                jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 3, "rut": rutLimpio},
                        function (medico) {
                            console.log(medico);
                            vrMedico = medico.rut_medico;
                        });

                if (vrMedico !== null) {
                    jQuery("#carg   andoAjax").css("visibility", "hidden");
                    alert("Registro Ya Existe");
                    return false;
                } else
                {

                    console.log(jQuery("select[name='txtespecialidad']").val());
                    vrRegMedico = null;
                    jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                            {"consulta": 11, "rut_medico": rutLimpio, "nombre_medico": jQuery("input[name='txtnombre_medico']").val(), "fecha_de_contratacion": jQuery("input[name='txtfecha_de_contratacion']").val(), "especialidad": jQuery("select[name='txtespecialidad']").val(), "valor_consulta": jQuery("input[name='txtvalor_consulta']").val()},
                            function (medico) {
                                vrRegMedico = medico;
                            });

                    jQuery("#cargandoAjax").css("visibility", "hidden");
                    if (vrRegMedico) {
                        alert("Médico Registrado");
                    } else
                    {
                        alert("Problemas al guardar Médico");
                        return false;

                    }
                }

            } else {
//                console.log("Rut no es valido...");
                alert("El rut ingresado no es válido");
                jQuery("input[name='txtrut']").css("border-color", "red");
                return false;
            }
        } else {
            jQuery("input[name='txtrut']").css("border-color", "initial");
            return false;
        }

    });

    /************************   Registrar Medico   ************************/




    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });




});


        