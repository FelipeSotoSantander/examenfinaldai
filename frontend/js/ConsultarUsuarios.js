jQuery(document).ready(function () {

    $.ajaxSetup({
        async: false
    });


    /************************   Cargar Perfil   ************************/

    jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
            {"consulta": 5},
            function (perfiles) {

                console.log(perfiles);

                jQuery("select[name='txtid_perfil'] option").remove();
                jQuery("select[name='txtid_perfil']").append("<option value=\"\">Seleccione Perfil</option>");

                jQuery.each(perfiles, function (indice, perfil) {
                    jQuery("select[name='txtid_perfil']").append("<option value=\"" + perfil.id_perfil + "\">" + perfil.nombre_perfil + "</option>");
                });


            });



    /************************   Consulta Usuario   ************************/


    jQuery("input[name='txtlogin_usuario']").blur(function () {

        if (this.value!=="") {



            jQuery("#cargandoAjax").css("visibility", "visible")
            jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                    {"consulta": 6, "usuario": this.value},
                    function (usuario) {

                        console.log(usuario);


                        jQuery("#cargandoAjax").css("visibility", "hidden");
                        jQuery("input[name='txtid_usuario']").css("border-color", "initial");
                        jQuery("input[name='txtid_usuario']").val(usuario.id_usuario);
                        jQuery("select[name='txtid_perfil']").val(usuario.id_perfil);
                        jQuery("input[name='txtnombre_usuario']").val(usuario.nombre_usuario);
                        jQuery("input[name='txtlogin_usuario']").val(usuario.login_usuario);
                        jQuery("input[name='txtclave_usuario']").val(usuario.clave_usuario);
                        jQuery("select[name='txtestado_usuario']").val(usuario.estado_usuario);

                        if (usuario.id_usuario == null) {
                            alert("Usuario No Encontrado")
                        }


                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        jQuery("#cargandoAjax").css("visibility", "hidden");
                        alert("Mensaje de error: " + err);
                    });

        } else {

            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtid_usuario']").val(this.value);
            jQuery("input[name='txtid_usuario']").css("border-color", "initial");
        }
    });







    /************************   Cargar Volver   ************************/

    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });




});


        