    
    
    jQuery(document).ready(function () {

        /************************   Cargar Perfil   ************************/

    jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
            {"consulta": 5},
            function (perfiles) {

                console.log(perfiles);

                jQuery("select[name='txtid_perfil'] option").remove();
                jQuery("select[name='txtid_perfil']").append("<option value=\"\">Seleccione Perfil</option>");

                jQuery.each(perfiles, function (indice, perfil) {
                    jQuery("select[name='txtid_perfil']").append("<option value=\"" + perfil.id_perfil + "\">" + perfil.nombre_perfil + "</option>");
                });
            });


    /************************   Consultar USuario   ************************/

    jQuery("input[name='txtlogin_usuario']").blur(function () {

        if (this.value!=="") {

            jQuery("#cargandoAjax").css("visibility", "visible")
            jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                    {"consulta": 6, "usuario": this.value},
                    function (usuario) {

                        console.log(usuario);

                        jQuery("#cargandoAjax").css("visibility", "hidden");
                        jQuery("input[name='txtid_usuario']").css("border-color", "initial");
                        jQuery("input[name='txtid_usuario']").val(usuario.id_usuario);
                        jQuery("select[name='txtid_perfil']").val(usuario.id_perfil);
                        jQuery("input[name='txtnombre_usuario']").val(usuario.nombre_usuario);
                        jQuery("input[name='txtlogin_usuario']").val(usuario.login_usuario);
                        jQuery("input[name='txtclave_usuario']").val(usuario.clave_usuario);
                        jQuery("select[name='txtestado_usuario']").val(usuario.estado_usuario);

                        if (usuario.id_usuario === null) {
                            alert("Usuario No Encontrado")
                        }


                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        jQuery("#cargandoAjax").css("visibility", "hidden");
                        alert("Mensaje de error: " + err);
                    });

        } else {

            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtid_usuario']").val(this.value);
            jQuery("input[name='txtid_usuario']").css("border-color", "initial");
        }
    });

    /************************   Consultar Usuario   ************************/

    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });
    /************************   Consultar Usuario   ************************/
        
    /************************   Eliminar usuario   ************************/
    jQuery("input[name='eliminar']").click(function () {

        vrIdUsuario = jQuery("input[name='txtid_usuario']").val();
        
        if (vrIdUsuario !== "") {
            jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 23, "idUsuario": vrIdUsuario},
                        function (usuario) {
                            console.log(usuario);


                            if (usuario) {
                                
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtlogin_usuario']").css("border-color", "initial");
                            jQuery("input[name='txtlogin_usuario']").val(usuario.login_usuario);
                            jQuery("select[name='txtid_perfil']").val(usuario.id_perfil);
                            jQuery("input[name='txtnombre_usuario']").val(usuario.nombre_usuario);
                            jQuery("input[name='txtclave_usuario']").val(usuario.clave_usuario);
                            jQuery("input[name='txtid_usuario']").val(usuario.id_usuario);
                            jQuery("select[name='txtestado_usuario']").val(usuario.estado_usuario);
                                
                            alert("Usuario eliminado");
                            }
                            else
                            {
                                  alert("No se elimino el Usuario");
                                
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            var err = textStatus + ", " + error;
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            alert("Mensaje de error: " + err);
                        });
            
        } else {
            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtid_usuario']").val(vrIdUsuario);
            jQuery("input[name='txtid_usuario']").css("border-color", "initial");
        }


    });
    /************************   Eliminar paciente   ************************/
});
