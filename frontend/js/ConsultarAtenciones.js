jQuery(document).ready(function () {

    $.ajaxSetup({
        async: false
    });


    /************************   Consultar Atenciones   ************************/


    jQuery("input[name='txtid_atencion']").blur(function () {
        vrIdAtencion = this.value;

        if (vrIdAtencion !== "") {

            jQuery("#cargandoAjax").css("visibility", "visible");

            jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                    {"consulta": 4, "idatencion": vrIdAtencion},
                    function (atencion) {
                        console.log(atencion);


                        if (atencion.id_atencion !== null) {
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtrut']").css("border-color", "initial");

                            jQuery("input[name='txtfecha']").val(atencion.fecha);

                            dvPaciente = $.Rut.getDigito(atencion.paciente);
                            rutPaciente = $.Rut.formatear(atencion.paciente + dvPaciente, true);

                            jQuery("input[name='txtpaciente']").val(rutPaciente);


                            dvMedico = $.Rut.getDigito(atencion.medico);
                            rutMedico = $.Rut.formatear(atencion.medico + dvMedico, true);

                            jQuery("input[name='txtmedico']").val(rutMedico);
                            jQuery("select[name='txtestado']").val(atencion.estado);


                        } else
                        {

                            jQuery("input[name='limpiar']").trigger("click");
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtid_atencion']").val(vrIdAtencion);
                            alert("Atencion No Encontrada");

                        }

                    })
                    .fail(function (jqxhr, textStatus, error) {
                        var err = textStatus + ", " + error;
                        jQuery("#cargandoAjax").css("visibility", "hidden");
                        alert("Mensaje de error: " + err);
                    });

        } else {

            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtid_atencion']").val(vrIdAtencion);
            jQuery("input[name='txtrut']").css("border-color", "initial");
        }

    });




    /************************   Actualizar Atenciones ************************/

    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });




});


        