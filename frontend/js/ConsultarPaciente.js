jQuery(document).ready(function () {


    /************************   Consultar Paciente   ************************/

    $("input[name='txtrut']").Rut({
        format_on: 'keyup'
    });

    jQuery("input[name='txtrut']").blur(function () {


        if (this.value !== "") {
            valido = jQuery.Rut.validar(this.value);
            if (valido) {
                jQuery("#cargandoAjax").css("visibility", "visible")
                //pasa el valor del campo rut a valorRut
                valorRut = this.value;
                //le quita los puntos y guion
                rutLimpio = $.Rut.quitarFormato(valorRut);
                //le quita el dv
                rutLimpio = rutLimpio.substring(0, rutLimpio.length - 1)
                //asigna el rutLimpio al campo rut
//                jQuery("input[name='rut']").val(rutLimpio);
                //pasa a buscar el JSON con los datos
                jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 2, "rut": rutLimpio},
                        function (paciente) {
                            console.log(paciente);


                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtrut']").css("border-color", "initial");
                            jQuery("input[name='txtnombre']").val(paciente.nombre_completo);
                            jQuery("input[name='txtfecha']").val(paciente.fecha_de_nacimiento);
                            jQuery("select[name='txtSexo']").val(paciente.sexo);
                            jQuery("input[name='txtDir']").val(paciente.direccion);
                            jQuery("input[name='txtFono']").val(paciente.telefono);

                            if (paciente.rut == null) {
                                alert("Paciente No Encontrado");
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            var err = textStatus + ", " + error;
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            alert("Mensaje de error: " + err);
                        });

            } else {
//                console.log("Rut no es valido...");
                alert("El rut ingresado no es valido");
                jQuery("#cargandoAjax").css("visibility", "hidden");
                jQuery("input[name='limpiar']").trigger("click");
                jQuery("input[name='txtrut']").val(this.value);
                jQuery("input[name='txtrut']").css("border-color", "red");
            }
        } else {

            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtrut']").val(this.value);
            jQuery("input[name='txtrut']").css("border-color", "initial");
        }

    });



    /************************   Volver   ************************/



    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });



});


        