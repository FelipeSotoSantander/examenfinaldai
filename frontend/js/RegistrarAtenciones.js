jQuery(document).ready(function () {

    $.ajaxSetup({
        async: false
    });
    $("input[name='txtpaciente']").Rut({
        format_on: 'keyup'
    });

    $("input[name='txtmedico']").Rut({
        format_on: 'keyup'
    });


    /************************   Registrar Atenciones ************************/

    jQuery("form[name='frmRegistrarAtenciones']").submit(function (evt) {

        vrrut_paciente = $("input[name='txtpaciente']").val()
        vrrut_medico = $("input[name='txtmedico']").val()

        if (!jQuery.Rut.validar(vrrut_paciente)) {
            alert("Rut Paciente No Valido");
            jQuery("input[name='txtpaciente']").css("border-color", "red");
            return false;
        }

        if (!jQuery.Rut.validar(vrrut_medico)) {
            alert("Rut Medico No Valido");
            jQuery("input[name='txtmedico']").css("border-color", "red");
            return false;
        }

//        jQuery("#cargandoAjax").css("visibility", "visible");

        rutLimpio_paciente = $.Rut.quitarFormato(vrrut_paciente);
        rutLimpio_paciente = rutLimpio_paciente.substring(0, rutLimpio_paciente.length - 1)


        rutLimpio_medico = $.Rut.quitarFormato(vrrut_medico);
        rutLimpio_medico = rutLimpio_medico.substring(0, rutLimpio_medico.length - 1)


        vrPaciente = null;
        jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                {"consulta": 2, "rut": rutLimpio_paciente},
                function (paciente) {
                    console.log(paciente);
                    vrPaciente = paciente.rut;
                });

        if (vrPaciente == null) {
            jQuery("#carg   andoAjax").css("visibility", "hidden");
            alert("Paciente No Existe");
            return false;
        }




        vrMedico = null;
        jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                {"consulta": 3, "rut": rutLimpio_medico},
                function (medico) {
                    console.log(medico);
                    vrMedico = medico.rut_medico;
                });

        if (vrMedico == null) {
            jQuery("#carg   andoAjax").css("visibility", "hidden");
            alert("Medico No Existe");
            return false;
        }


        console.log(jQuery("select[name='txtespecialidad']").val());
        vrReg = null;
        jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                {"consulta": 12, "fecha" : jQuery("input[name='txtfecha']").val(), "paciente" : rutLimpio_paciente, "medico" : rutLimpio_medico, "estado" : jQuery("select[name='txtestado']").val()},
                function (registro) {
                    vrReg = registro;
                });

        jQuery("#cargandoAjax").css("visibility", "hidden");

        if (vrReg) {
            alert("Atención Registrada");
        } else
        {
            alert("Problemas al guardar Atención");
            return false;

        }


    });

    




    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });




});


        