    
    
    jQuery(document).ready(function () {

    /************************   Consultar Medico   ************************/

    $("input[name='txtrut_medico']").Rut({
        format_on: 'keyup'
    });

    jQuery("input[name='txtrut_medico']").blur(function () {
        if (this.value !== "") {
            valido = jQuery.Rut.validar(this.value);
            if (valido) {
                jQuery("#cargandoAjax").css("visibility", "visible")
                //pasa el valor del campo rut a valorRut
                valorRut = this.value;
                //le quita los puntos y guion
                rutLimpio = $.Rut.quitarFormato(valorRut);
                //le quita el dv
                rutLimpio = rutLimpio.substring(0, rutLimpio.length - 1)
                //asigna el rutLimpio al campo rut
//                jQuery("input[name='rut']").val(rutLimpio);
                //pasa a buscar el JSON con los datos
                jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 3, "rut": rutLimpio},
                        function (medico) {
                            console.log(medico);

                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtnombre_medico']").css("border-color", "initial");
                            jQuery("input[name='txtnombre_medico']").val(medico.nombre_medico);
                            jQuery("input[name='txtfecha_de_contratacion']").val(medico.fecha_de_contratacion);
                            jQuery("input[name='txtespecialidad']").val(medico.especialidad);
                            jQuery("input[name='txtvalor_consulta']").val(medico.valor_consulta);

                            if (medico.rut_medico == null) {
                                alert("Medico No Encontrado");
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            var err = textStatus + ", " + error;
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            alert("Mensaje de error: " + err);
                        });

            } else {
//                console.log("Rut no es valido...");
                alert("El rut ingresado no es valido");
                jQuery("#cargandoAjax").css("visibility", "hidden");
                jQuery("input[name='limpiar']").trigger("click");
                jQuery("input[name='txtrut_medico']").val(this.value);
                jQuery("input[name='txtrut_medico']").css("border-color", "red");


            }
        } else {

            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtrut_medico']").val(this.value);
            jQuery("input[name='txtrut_medico']").css("border-color", "initial");
        }

    });


    /************************   Consultar Medico   ************************/

    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });

    jQuery("input[name='eliminar']").click(function () {

        vrRut = jQuery("input[name='txtrut_medico']").val();
        
        if (vrRut !== "") {
            valido = jQuery.Rut.validar(vrRut);
            if (valido) {
                jQuery("#cargandoAjax").css("visibility", "visible")
                //pasa el valor del campo rut a valorRut
                valorRut = vrRut;
                //le quita los puntos y guion
                rutLimpio = $.Rut.quitarFormato(valorRut);
                //le quita el dv
                rutLimpio = rutLimpio.substring(0, rutLimpio.length - 1)
                //asigna el rutLimpio al campo rut
                //jQuery("input[name='rut']").val(rutLimpio);
                //pasa a buscar el JSON con los datos
                jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 21, "rut": rutLimpio},
                        function (medico) {
                            console.log(medico);


                            if (medico) {
                                
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtrut_medico']").css("border-color", "initial");
                            jQuery("input[name='txtrut_medico']").val(medico.rut);
                            jQuery("input[name='txtnombre_medico']").val(medico.nombre_completo);
                            jQuery("input[name='txtfecha_de_contratacion']").val(medico.fecha_de_contratacion);
                            jQuery("input[name='txtespecialidad']").val(medico.especialidad);
                            jQuery("input[name='txtvalor_consulta']").val(medico.valor_consulta);
                                alert("Médico Eliminado");
                            }
                            else
                            {
                                  alert("No Se elimino el Médico");
                                
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            var err = textStatus + ", " + error;
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            alert("Mensaje de error: " + err);
                        });

            } else {
//                console.log("Rut no es valido...");
                alert("El rut ingresado no es valido");
                jQuery("#cargandoAjax").css("visibility", "hidden");
                jQuery("input[name='limpiar']").trigger("click");
                jQuery("input[name='txtrut_medico']").val(vrRut);
                jQuery("input[name='txtrut_medico']").css("border-color", "red");
            }
        } else {

            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtrut_medico']").val(vrRut);
            jQuery("input[name='txtrut_medico']").css("border-color", "initial");
        }


    });

});
