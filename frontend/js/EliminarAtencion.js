    jQuery(document).ready(function () {

        /************************   Consultar Atenciones   ************************/
    
    jQuery("input[name='txtid_atencion']").blur(function () {
            vrIdAtencion = this.value;

        if (vrIdAtencion !== "") {

                jQuery("#cargandoAjax").css("visibility", "visible");
                
                jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 4, "idatencion": vrIdAtencion},
                        function (atencion) {
                            console.log(atencion);

                            if (atencion.id_atencion !== null) {
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtid_atencion']").css("border-color", "initial");
                                                        
                            jQuery("input[name='txtFechaAtencion']").val(atencion.fecha);
                            
                            dvPaciente = $.Rut.getDigito(atencion.paciente);
                            console.log("dvPaciente  ",dvPaciente);
                            rutPaciente = $.Rut.formatear(atencion.paciente + dvPaciente, true);
                            console.log("rutPaciente  ",rutPaciente);
                            jQuery("input[name='txtRutPaciente']").val(rutPaciente);
                            
                            
                            dvMedico = $.Rut.getDigito(atencion.medico);
                            rutMedico  = $.Rut.formatear(atencion.medico + dvMedico, true);
                            
                            jQuery("input[name='txtRutMedico']").val(rutMedico);
                            jQuery("input[name='txtEstado']").val(atencion.estado);


                            }
                            else
                            {
                                
                                jQuery("input[name='limpiar']").trigger("click");
                                jQuery("#cargandoAjax").css("visibility", "hidden");
                                jQuery("input[name='txtid_atencion']").val(vrIdAtencion);
                                alert("Atencion No Encontrada");
                                
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            var err = textStatus + ", " + error;
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            alert("Mensaje de error: " + err);
                        });

        } else {

            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtid_atencion']").val(vrIdAtencion);
            jQuery("input[name='txtid_atencion']").css("border-color", "initial");
        }

    });


    /************************   Consultar Atenciones   ************************/

    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });

        
    /************************   Inicio - Eliminar atencion   ************************/
    jQuery("input[name='eliminar']").click(function () {

        vrIdAtencion = jQuery("input[name='txtid_atencion']").val();
        
        if (vrIdAtencion !== "") {
            jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 22, "idAtencion": vrIdAtencion},
                        function (atencion) {
                            console.log(atencion);


                            if (atencion) {
                                
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtid_atencion']").css("border-color", "initial");
                            jQuery("input[name='txtid_atencion']").val(atencion.id_atencion);
                            jQuery("input[name='txtFechaAtencion']").val(atencion.fecha);
                            jQuery("input[name='txtRutPaciente']").val(atencion.paciente);
                            jQuery("input[name='txtRutMedico']").val(atencion.medico);
                            jQuery("input[name='txtEstado']").val(atencion.estado);

                                alert("Atención eliminada");
                            }
                            else
                            {
                                  alert("No se elimino la Atención");
                                
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            var err = textStatus + ", " + error;
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            alert("Mensaje de error: " + err);
                        });
            
        } else {
            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtid_atencion']").val(vrIdAtencion);
            jQuery("input[name='txtid_atencion']").css("border-color", "initial");
        }


    });
    /************************  Fin - Eliminar Atencion   ************************/
});
