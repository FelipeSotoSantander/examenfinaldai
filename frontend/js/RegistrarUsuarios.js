jQuery(document).ready(function () {

    $.ajaxSetup({
        async: false
    });

    $("input[name='txtrut']").Rut({
        format_on: 'keyup'
    });


    jQuery("#divRutPaciente").hide();
    /************************   Seleccionar Perfil   ************************/


    jQuery("select[name='txtid_perfil']").change(function () {
        if (this.value == 4) {
            jQuery("#divRutPaciente").show();
            jQuery("input[name='txtrut']").attr("type", "text");
            jQuery("input[name='txtrut']").attr("required", true);
        } else {

            jQuery("#divRutPaciente").hide();
            jQuery("input[name='txtrut']").attr("type", "hidden");
            jQuery("input[name='txtrut']").attr("required", false);

        }


    });





    /************************   Cargar Perfil   ************************/

    jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
            {"consulta": 5},
            function (perfiles) {

                console.log(perfiles);

                jQuery("select[name='txtid_perfil'] option").remove();
                jQuery("select[name='txtid_perfil']").append("<option value=\"\">Seleccione Perfil</option>");

                jQuery.each(perfiles, function (indice, perfil) {
                    jQuery("select[name='txtid_perfil']").append("<option value=\"" + perfil.id_perfil + "\">" + perfil.nombre_perfil + "</option>");
                });


            });


    /************************   Registrar Usuario  ************************/

    jQuery("form[name='frmRegistrarUsuarios']").submit(function (evt) {


        rutLimpio_paciente = null;

        if (jQuery("select[name='txtid_perfil']").val() == "4") {
            vrrut_paciente = $("input[name='txtrut']").val();

            if (!jQuery.Rut.validar(vrrut_paciente)) {
                alert("Rut Paciente No Valido");
                jQuery("input[name='txtrut']").css("border-color", "red");
                return false;
            }

            rutLimpio_paciente = $.Rut.quitarFormato(vrrut_paciente);
            rutLimpio_paciente = rutLimpio_paciente.substring(0, rutLimpio_paciente.length - 1)


            vrPaciente = null;
            jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                    {"consulta": 2, "rut": rutLimpio_paciente},
                    function (paciente) {
                        console.log(paciente);
                        vrPaciente = paciente.rut;
                    });

            if (vrPaciente == null) {
                jQuery("#carg   andoAjax").css("visibility", "hidden");
                alert("Paciente No Existe");
                return false;
            }

 

            vrPacienteUsuario = null;
            jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                    {"consulta": 8, "rut": rutLimpio_paciente},
                    function (usuario) {
                        console.log(usuario);
                        vrPacienteUsuario = usuario.rut;
                    });

            if (vrPacienteUsuario !== null) {
                jQuery("#carg   andoAjax").css("visibility", "hidden");
                alert("Paciente Ya Tiene Login");
                return false;
            }





        }


        vrLogin = null;
        jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                {"consulta": 6, "usuario": jQuery("input[name='txtlogin_usuario']").val()},
                function (usuario) {
                    vrLogin = usuario.id_usuario;
                });

        if (vrLogin !== null) {
            jQuery("#cargandoAjax").css("visibility", "hidden");
            alert("Login ya Existe");
            return false;
        }





        vrReg = null;
        jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                {"consulta": 13, "id_perfil": jQuery("select[name='txtid_perfil']").val(), "nombre_usuario": jQuery("input[name='txtnombre_usuario']").val(), "login_usuario": jQuery("input[name='txtlogin_usuario']").val(), "clave_usuario": jQuery("input[name='txtclave_usuario']").val(), "estado_usuario": jQuery("select[name='txtestado_usuario']").val(), "rut": rutLimpio_paciente},
                function (registro) {
                    vrReg = registro;
                });

        jQuery("#cargandoAjax").css("visibility", "hidden");

        if (vrReg) {
            alert("Usuario Registrado OK");
        } else
        {
            alert("Problemas al guardar Usuario");
            return false;

        }


    });



    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });




});


        