jQuery(document).ready(function () {



    /************************   Consultar Paciente   ************************/
    $.ajaxSetup({
        async: false
    });
    $("input[name='txtrutreg']").Rut({
        format_on: 'keyup'
    });

    //////////////////////////////////


    jQuery("form[name='frmRegistrarPaciente']").submit(function (evt) {

        vrRut = $("input[name='txtrutreg']").val()
        if (vrRut !== "") {
            valido = jQuery.Rut.validar(vrRut);
            if (valido) {
//                alert("aqui");
                jQuery("#cargandoAjax").css("visibility", "visible");
                //pasa el valor del campo rut a valorRut
                valorRut = vrRut;
                //le quita los puntos y guion
                rutLimpio = $.Rut.quitarFormato(valorRut);
                //le quita el dv
                rutLimpio = rutLimpio.substring(0, rutLimpio.length - 1)
                //asigna el rutLimpio al campo rut
//                jQuery("input[name='rut']").val(rutLimpio);
                //pasa a buscar el JSON con los datos

                jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 2, "rut": rutLimpio},
                        function (paciente) {
                            vrPaciente = paciente.rut;
                        });

                if (vrPaciente !== null) {
                    jQuery("#cargandoAjax").css("visibility", "hidden");
                    alert("Registro Ya Existe");
                    return false;
                } else
                {


//                    $rut, $nombre_completo, $fecha_de_nacimiento, $sexo, $direccion, $telefono
                    vrRegPaciente = null;
                    jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                            {"consulta": 10, "rut": rutLimpio, "nombre_completo": jQuery("input[name='txtnombre']").val(), "fecha_de_nacimiento": jQuery("input[name='txtfecha']").val(), "sexo": jQuery("select[name='txtsexo']").val(), "direccion": jQuery("input[name='txtdir']").val(), "telefono": jQuery("input[name='txtfono']").val()},
                            function (paciente) {
                                vrRegPaciente = paciente;
                            });

                    jQuery("#cargandoAjax").css("visibility", "hidden");
                    if (vrRegPaciente) {
                        alert("Paciente Registrado");
                    } else
                    {
                        alert("Problemas al guardar Paciente");
                        return false;

                    }
                }

            } else {
//                console.log("Rut no es valido...");
                alert("El rut ingresado no es valido");
                jQuery("input[name='txtrutreg']").css("border-color", "red");
                return false;
            }
        } else {
            jQuery("input[name='txtrutreg']").css("border-color", "initial");
            return false;
        }

    });

   
    
    
    

    /************************   Consultar Volver   ************************/
    



    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });




});


        