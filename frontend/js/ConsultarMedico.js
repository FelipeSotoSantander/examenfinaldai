jQuery(document).ready(function () {

    $("input[name='txtrut_medico']").Rut({
        format_on: 'keyup'
    });
    
    
    
    /************************   Cargar Especialidades   ************************/

    jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
            {"consulta": 7},
            function (especialidades) {

                console.log(especialidades);

                jQuery("select[name='txtespecialidad'] option").remove();
                jQuery("select[name='txtespecialidad']").append("<option value=\"\">Seleccione Especialidad</option>");

                jQuery.each(especialidades, function (indice, especialidad) {
                    jQuery("select[name='txtespecialidad']").append("<option value=\"" + especialidad.especialidad + "\">" + especialidad.nombre_especialidad + "</option>");
                });


            });
    /************************   Consultar Medico   ************************/



    jQuery("input[name='txtrut_medico']").blur(function () {


        if (this.value !== "") {
            valido = jQuery.Rut.validar(this.value);
            if (valido) {
                jQuery("#cargandoAjax").css("visibility", "visible")
                //pasa el valor del campo rut a valorRut
                valorRut = this.value;
                //le quita los puntos y guion
                rutLimpio = $.Rut.quitarFormato(valorRut);
                //le quita el dv
                rutLimpio = rutLimpio.substring(0, rutLimpio.length - 1)
                //asigna el rutLimpio al campo rut
//                jQuery("input[name='rut']").val(rutLimpio);
                //pasa a buscar el JSON con los datos
                jQuery.getJSON("../backend/ConsultasAjax/Consultas.php",
                        {"consulta": 3, "rut": rutLimpio},
                        function (medico) {
                            console.log(medico);

                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            jQuery("input[name='txtnombre_medico']").css("border-color", "initial");
                            jQuery("input[name='txtnombre_medico']").val(medico.nombre_medico);
                            jQuery("input[name='txtfecha_de_contratacion']").val(medico.fecha_de_contratacion);
                            
                            console.log(medico.especialidad);
                            jQuery("select[name='txtespecialidad']").val(medico.especialidad);
                            jQuery("input[name='txtvalor_consulta']").val(medico.valor_consulta);

                            if (medico.rut_medico == null) {
                                alert("Medico No Encontrado");
                            }

                        })
                        .fail(function (jqxhr, textStatus, error) {
                            var err = textStatus + ", " + error;
                            jQuery("#cargandoAjax").css("visibility", "hidden");
                            alert("Mensaje de error: " + err);
                        });

            } else {
//                console.log("Rut no es valido...");
                alert("El rut ingresado no es valido");
                jQuery("#cargandoAjax").css("visibility", "hidden");
                jQuery("input[name='limpiar']").trigger("click");
                jQuery("input[name='txtrut_medico']").val(this.value);
                jQuery("input[name='txtrut_medico']").css("border-color", "red");


            }
        } else {

            jQuery("input[name='limpiar']").trigger("click");
            jQuery("input[name='txtrut_medico']").val(this.value);
            jQuery("input[name='txtrut_medico']").css("border-color", "initial");
        }

    });


    /************************  Volver   ************************/



    jQuery("input[name='volver']").click(function () {
        window.location.href = "Menu.php";
    });




});


        