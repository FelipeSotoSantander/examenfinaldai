<?php

session_start();

include_once "/../backend/Controller/PacienteController.php";
$pacientes = PacienteController::ListarPaciente();

?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/ListarPaciente.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Listar Paciente</h1>
            </header>
            <div id="contenido">
                <form action="Login.php" method="POST" name="formulario" >
                    <fieldset>
                        <legend>Pacientes</legend>
                        <div class="campoFormulario">
                            <table>
                                <tr>
                                    <td with="15%">RUT</td>
                                    <td with="20%">NOMBRE_COMPLETO</td>
                                    <td with="10%">FECHA_DE_NACIMIENTO</td>
                                    <td with="15%">SEXO</td>
                                    <td with="30%">DIRECCION</td>
                                    <td with="10%">TELEFONO</td>
                                </tr>
                                <?php
                                foreach ($pacientes as $value) {
                                    ?>
                                <tr>
                                    <td class="rut" ><?=$value["rut"]?></td>
                                    <td><?=$value["nombre_completo"]?></td>
                                    <td><?=$value["fecha_de_nacimiento"]?></td>
                                    <td><?=$value["sexo"]?></td>
                                    <td><?=$value["direccion"]?></td>
                                    <td><?=$value["telefono"]?></td>
                                </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                        
                    <div class="botonera">
                        <input type="button" value="Volver" name="volver" />
                    </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
