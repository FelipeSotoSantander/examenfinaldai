<?php
session_start();
?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/RegistrarPaciente.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Registrar Paciente</h1>
            </header>
            <div id="contenido">
                <div id="cargandoAjax">
                    <img src="img/ajax-loader.gif" alt="cargando..."/>
                </div>
                <form action="RegistrarPaciente.php" method="POST" name="frmRegistrarPaciente" id="frmRegistrarPaciente">
                    <fieldset>
                        <legend>Paciente</legend>
                        <div class="campoFormulario">
                            Rut:
                            <input id="txtrutreg" name="txtrutreg" type="text" class="rut" placeholder="xx.xxx.xxx-x" required/>
                            Nombre:
                            <input id="txtnombre" name="txtnombre" type="text" required/>
                            Fecha de Nacimiento:
                            <input id="txtfecha" name="txtfecha"  type="date" required/>

                            Sexo:

                            <div class="styled-select slate">
                                <select id="txtsexo" name="txtsexo" required>
                                    <option value="">Seleccione</option>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                </select>
                            </div>


                            Dirección:
                            <input id="txtdir" name="txtdir" type="text" required/>
                            Teléfono:
                            <input id="txtfono" name="txtfono" type="text" required/>
                        </div>
                        <br/>
                        <div class="botonera">
                            <input type="reset" value="Limpiar" name="limpiar" />
                            <input type="submit" value="Registrar" name="registrarPaciente" />
                            <input type="button" value="Volver" name="volver" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
