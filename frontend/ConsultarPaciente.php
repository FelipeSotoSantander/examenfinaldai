<?php
session_start();

?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/ConsultarPaciente.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Consultar Paciente</h1>
            </header>
            <div id="contenido">
                 <div id="cargandoAjax">
                    <img src="img/ajax-loader.gif" alt="cargando..."/>
                </div>
                <form action="Login.php" method="POST" name="formulario" >
                    <fieldset>
                        <legend>Paciente</legend>
                        <div class="campoFormulario">
                            Rut:
                            <input id="txtrut" name="txtrut" type="text" class="rut" placeholder="xx.xxx.xxx-x" required/>
                            Nombre:
                            <input id="txtnombre" name="txtnombre" type="text" readonly/>
                            Fecha de Nacimiento:
                            <input id="txtfecha" name="txtfecha"  type="date" readonly/>
                            Sexo:
                            
                            <div class="styled-select slate">
                                <select id="txtSexo" name="txtSexo" disabled="true">
                                    <option value="">Seleccione</option>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                </select>
                            </div>

                            Direccion:
                            <input id="txtDir" name="txtDir" type="text" readonly/>
                            Telefono:
                            <input id="txtFono" name="txtFono" type="text" readonly/>


                            
                        </div>
                        <br/>
                        <div class="botonera">
                            
                            <input type="reset" value="Limpiar" name="limpiar" />                            
                            <input type="button" value="Volver" name="volver" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
