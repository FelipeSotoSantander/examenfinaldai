<?php
session_start();

?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/EliminarAtencion.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Eliminar Atenciones</h1>
            </header>
            <div id="contenido">
                 <div id="cargandoAjax">
                    <img src="img/ajax-loader.gif" alt="cargando..."/>
                </div>
                <form action="#" method="POST" name="formulario" >
                    <fieldset>
                        <legend>Eliminar Atención</legend>
                        <div class="campoFormulario">
                            Número de Atención:
                            <input id="txtid_atencion" name="txtid_atencion" type="text" class="rut" placeholder="xx.xxx.xxx-x" required/>
                            Fecha:
                            <input id="txtFechaAtencion" name="txtFechaAtencion" type="date" readonly/>
                            Rut Paciente:
                            <input id="txtRutPaciente" name="txtRutPaciente"  placeholder="xx.xxx.xxx-x" type="text" readonly/>
                            Rut Médico:
                            <input id="txtRutMedico" name="txtRutMedico" placeholder="xx.xxx.xxx-x" type="text" readonly/>
                            Estado:
                            <input id="txtEstado" name="txtEstado" type="text" readonly/>
                        </div>
                        <br/>
                        <div class="botonera">
                            
                            <input type="reset" value="Limpiar" name="limpiar" />
                            <input type="button" value="Eliminar" name="eliminar" />                            
                            <input type="button" value="Volver" name="volver" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
