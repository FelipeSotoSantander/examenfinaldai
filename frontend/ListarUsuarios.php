<?php

session_start();

include_once "/../backend/Controller/UsuarioController.php";
$usuarios = UsuarioController::ListarUsuarioCompleto();


?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/BotonVolver.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Listar Usuarios</h1>
            </header>
            <div id="contenido">
                <form action="Login.php" method="POST" name="formulario" >
                    <fieldset>
                        <legend>Usuarios</legend>
                        <div class="campoFormulario">
                            <table>
                                <tr>
                                    <td with="10%">ID_USUARIO</td>
                                    <td with="10%">PERFIL</td>
                                    <td with="10%">NOMBRE_USUARIO</td>
                                    <td with="10%">LOGIN_USUARIO</td>
                                    <td with="10%">CLAVE_USUARIO</td>
                                    <td with="10%">ESTADO_USUARIO</td>
                                </tr>
                                <?php
                                foreach ($usuarios as $value) {
                                    ?>
                                <tr>
                                    <td><?=$value["id_usuario"]?></td>
                                    <td><?=$value["id_perfil"]["nombre_perfil"]?></td>
                                    <td><?=$value["nombre_usuario"]?></td>
                                    <td><?=$value["login_usuario"]?></td>
                                    <td><?=$value["clave_usuario"]?></td>
                                    <td><?php
                                            if ($value["estado_usuario"]==1) {
                                                echo "Habilitado";
                                            }else{
                                                echo "Deshabilitado";
                                                
                                            }
                                                
                                    ?></td>
                                </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                        
                    <div class="botonera">
                        <input type="button" value="Volver" name="volver" />
                    </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
