<?php

session_start();

include_once "/../backend/Controller/AtencionController.php";
$atenciones = AtencionController::ListarAtencionCompletoPorUsuario($_SESSION['usuario']["rut"]);


?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/ListarAtencionesPorPaciente.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Listar Atenciones</h1>
            </header>
            <div id="contenido">
                <form action="Login.php" method="POST" name="formulario" >
                    <fieldset>
                        <legend>Atenciones</legend>
                        <div class="campoFormulario">
                            <table>
                                <tr>
                                    <td with="15%">ID_ATENCION</td>
                                    <td with="20%">FECHA</td>
                                    <td with="10%">PACIENTE</td>
                                    <td with="15%">MEDICO</td>
                                    <td with="30%">ESTADO</td>
                                </tr>
                                <?php
                                foreach ($atenciones as $value) {
                                    ?>
                                <tr>
                                    <td><?=$value["id_atencion"]?></td>
                                    <td><?=$value["fecha"]?></td>
                                    <td><?=$value["paciente"]["nombre_completo"]?></td>
                                    <td><?=$value["medico"]["nombre_medico"]?></td>
                                    <td><?=$value["estado"]?></td>                                    
                                </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                        
                    <div class="botonera">
                        <input type="button" value="Volver" name="volver" />
                    </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
