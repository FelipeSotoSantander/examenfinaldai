<?php
session_start();
?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/RegistrarUsuarios.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Registrar Usuario</h1>
            </header>
            <div id="contenido">
                <div id="cargandoAjax">
                    <img src="img/ajax-loader.gif" alt="cargando..."/>
                </div>
                <form action="RegistrarUsuarios.php" method="POST" name="frmRegistrarUsuarios" id="frmRegistrarUsuarios">
                    <fieldset>
                        <legend>Usuario</legend>
                        <div class="campoFormulario">
                            Perfil:                             
                            <div class="styled-select slate">
                                <select id="txtid_perfil" name="txtid_perfil" >
                                    <option value="">Seleccione</option>
                                    <option value="director">Director</option>
                                    <option value="administrador">Administrador</option>
                                    <option value="secretaria">Secretaria</option>
                                    <option value="paciente">Paciente</option>                                    
                                </select>
                            </div>
                        </div>
                        Nombre Completo: 
                        <input id="txtnombre_usuario" name="txtnombre_usuario" type="text" required/>
                        Usuario: 
                        <input id="txtlogin_usuario" name="txtlogin_usuario" type="text" required/>
                        Password: 
                        <input id="txtclave_usuario" name="txtclave_usuario" type="password" required/>
                        Estado: 
                        <div class="styled-select slate">
                            <select id="txtestado_usuario" name="txtestado_usuario" required>
                                <option value="">Seleccione Estado</option>
                                <option value="1">Habilitado</option>
                                <option value="0">Deshabilitado</option>
                            </select>
                        </div>

                        <div id="divRutPaciente">
                            Rut Paciente:
                            <input id="txtrut" name="txtrut" type="hidden" class="rut" placeholder="xx.xxx.xxx-x" />
                        </div>


                        </div>
                        <br/>
                        <div class="botonera">

                            <input type="reset" value="Limpiar" name="limpiar" />
                            <input type="submit" value="Registrar" name="registrarPaciente" />
                            <input type="button" value="Volver" name="volver" />
                        </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
