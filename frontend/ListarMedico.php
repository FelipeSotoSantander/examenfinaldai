<?php

session_start();

include_once "/../backend/Controller/MedicoController.php";
$medicos = MedicoController::ListarMedicoCompleto();

?>


<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
        <script src="js/jquery-3.2.1.js" ></script>
        <script src="js/jquery.rut.js" ></script>
        <script src="js/ListarMedico.js" ></script>
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />


    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Listar Médicos</h1>
            </header>
            <div id="contenido">
                <form action="Login.php" method="POST" name="formulario" >
                    <fieldset>
                        <legend>Medico</legend>
                        <div class="campoFormulario">
                            <table>
                                <tr>
                                    <td with="15%">RUT_MEDICO</td>
                                    <td with="20%">NOMBRE_MEDICO</td>
                                    <td with="10%">FECHA_DE_CONTRATACION</td>
                                    <td with="15%">ESPECIALIDAD</td>
                                    <td with="30%">VALOR_CONSULTA</td>
                                </tr>
                                <?php
                                foreach ($medicos as $value) {
                                    ?>
                                <tr>
                                    <td class="rut"><?=$value["rut_medico"]?></td>
                                    <td><?=$value["nombre_medico"]?></td>
                                    <td><?=$value["fecha_de_contratacion"]?></td>
                                    <td><?=$value["especialidad"]["nombre_especialidad"]?></td>
                                    <td><?=$value["valor_consulta"]?></td>                                    
                                </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                        
                    <div class="botonera">
                        <input type="button" value="Volver" name="volver" />
                    </div>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
