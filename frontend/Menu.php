<?php
session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once "/../backend/Controller/PermisoController.php";
include_once "/../backend/Controller/PrivilegioController.php";




if (isset($_SESSION['usuario'])) {

    $permisos = PermisoController::ConsultarPermiso($_SESSION['usuario']['id_perfil']);
}
?>

<!DOCTYPE html>


<html>
    <head>
        <title>Isapre Somos Salud</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" media="all" href="css/principal.css" />
    </head>
    <body>
        <div id="contenedor">
            <header>
                <h1>Menú del Sistema</h1>
            </header>
            <div id="contenidoMenu">
                <form action="#" method="POST" name="formularioMenu" >
                    <fieldset>
                        <legend></legend>
                        <ul>

                        <?php
                        foreach ($permisos as $value) {
                            ?>
                            <li>
                            <div class="campoFormulario">
                                <a href="<?=$value["privilegio"]["url_privilegio"]?>" >
                                    <?=$value["privilegio"]["nombre_privilegio"]?>
                                </a>
                            </div>
                            </li>
                        <?php
                        }
                        ?>
                        </ul>
                    </fieldset>
                </form>
            </div>
            <footer>
                <p>Diseño de Aplicaciones para Internet</p>
            </footer>
        </div>
    </body>
</html>
